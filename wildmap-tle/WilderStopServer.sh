
echo "stop all servers"
pm2 stop tleserver wildmaphttp wildmaptile wildsyncserver

echo "delete all servers"
pm2 delete tleserver wildmaphttp wildmaptile wildsyncserver

echo "killall tile servers on nodes"
walldo killall node
