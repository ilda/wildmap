
offsetX=0
offsetY=0
wcol=2
wrow=5
wtile_wa=7680
wtile_wb=6720
wtile_h=960
viewSyncPort=8080
serverport=8081
tuioport=8083
nwpath="nw"
user=wild
wallwidth=14400
wallheight=4800
#proxyaddress="192.168.2.254:8086"

#master canvas size. MUST be the same ratio than the wall
masterwidth=1440
masterheight=480


packagefile="package.json"
nodetmppath="/home/wild/tmp/wildmap"
nodeuserdirpath="/home/wild/.config/wildmap"

#change those values
servertmppath="/home/wild/tmp/tilemap/package.json"
serverip="192.168.2.1"
tilesourcecluster="127.0.0.1:8082/darkmatter-world-nolb"
tilesourcemaster="192.168.2.1:8082/darkmatter-world-nolb"
tileext="png"

clientcachedir="/media/ssd2To/cache/wildmap"



echo "clear nw cache"

walldo rm -r $nodeuserdirpath
walldo rm -r $nodetmppath
walldo rm -r $clientcachedir
sleep 1
walldo mkdir $nodetmppath
walldo mkdir $clientcachedir

sleep 1

wtile_w=$wtile_wa
echo "start nw on nodes"
for col in {a..b}
do
  for row in {1..5}
    do
      echo  {\"name\": \"wildmap\",\"nodejs\": \"true\",\"main\": "\"http://$serverip:$serverport/?id=$col$row&offsetx=$offsetX&offsety=$offsetY&wallx=$wcol&wally=$wrow&wallwidth=$wallwidth&wallheight=$wallheight&sizex=$wtile_w&sizey=$wtile_h&tilesource=http://$tilesourcecluster&syncserver=$serverip&viewSyncPort=$viewSyncPort&tileext=$tileext\"",\"window\": {\"kiosk\": true}} > $servertmppath$col$row
      scp $servertmppath$col$row $user@$col$row:$nodetmppath/$packagefile
      ssh $user@$col$row DISPLAY=:0 $nwpath $nodetmppath --disk-cache-dir=$clientcachedir&
      sleep 0.2
      offsetY=$((offsetY+wtile_h))
    done
    offsetX=$((offsetX+wtile_wa))
    offsetY=0
    wtile_w=$wtile_wb
done

echo "Click on the link to launch master locally"

echo http://$serverip:$serverport/?id=MASTER\&sizex=$masterwidth\&sizey=$masterheight\&wallwidth=$wallwidth\&wallheight=$wallheight\&syncserver=$serverip\&viewSyncPort=$viewSyncPort\&tilesource=http://$tilesourcemaster\&tuioserver=$serverip\&tuioport=$tuioport\&tileext=$tileext
