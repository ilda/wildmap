# wildmap app for TLE data

TLE satellite visualization running on cluster-driven UHRWD using Web technologies.


Uses [OpenSeaDragon](https://openseadragon.github.io/) to display TMS tiles. Uses both WebGL and svg layers to display data on top of the tile pyramid.

## Installation

In both `server` and `client` directories, run:

```npm install```

```npm run compile```

Make sure that the serve module is installed on both the master and slave computers:

```npm install -g serve```

Make sure the pm2 module is installed on the master computer:

```npm install -g pm2```

## Usage

### URL parameters (client)

Example (master,local):

```http://127.0.0.1:8081/index.html?id=MASTER&sizex=800&sizey=600&tilesource=http://127.0.0.1:8082&satSymbolSize=100&infoBoxScale=2&satStrokeSize=2&satPathStrokeSize=2```

* Satellite symbol size (make them larger on wild than wilder). Default: 40.

```&satSymbolSize=60```

* Satellite symbol stroke size (make it larger on wild than wilder). Default: 1.

```&satStrokeSize=2```

* Satellite path stroke size (make it larger on wild than wilder). Default: 1.

```&satPathStrokeSize=2```

* infobox scale factor (make them larger on wild than wilder). Default: 1.

```&infoBoxScale=1.5```

### command line arguments (server)

* trace level (log) between 0 and 2, default=1

```--tl=2```

## License

OpenSeaDragon is released under the New BSD License https://openseadragon.github.io/license/
wildmap-osd is designed for the Wild/Wilder platforms and therefore not intended to be publicly released
