import OpenSeadragon, {Placement, Point} from "openseadragon";
import io from "socket.io-client";
import * as d3 from "d3";
import Color from "color";
import {WildOsdOptions, WildOsd, Rpc, WebGlOverlay, Canvas2dOverlay, PieMenu, SvgOverlay} from "wildosd";
import randomColor from "randomcolor";

interface SATCATInfo {
	OBJECT_NAME:string;
	OBJECT_ID:string;
	NORAD_CAT_ID:string;
	OBJECT_TYPE:string;
	OPS_STATUS_CODE:string;
	OWNER:string;
	LAUNCH_DATE:string;
	LAUNCH_SITE:string;
	DECAY_DATE:string;
	PERIOD:string;
	INCLINATION:string;
	APOGEE:string;
	PERIGEE:string;
	RCS:string;
	DATA_STATUS_CODE:string;
	ORBIT_CENTER:string;
	ORBIT_TYPE:string;
};

interface SatPosInfo {
	lat:number;
	lon:number;
	group?:string;
	satrec?:Object;
};

type LiveSatInfo = SATCATInfo & SatPosInfo;

// when InfoBox is manually repositioned on map,
// store that offset so that it can be enforced
// when zooming
interface InfoBoxOffset {
	xo: number;
	yo: number;
}

const InfoBox = {
	ID_PREFIX: "ib",
	WIDTH: 256,
	SAT_THUMB_WIDTH: 256 / 1.1,
	HEIGHT: 50,
	MARGIN_LEFT: 14,
	MARGIN_RIGHT: 24,
	SCALE_FACTOR: 0.5,
	DEFAULT_X_OFFSET: 10,
	DEFAULT_Y_OFFSET: 10,
	X_OFFSET: 10,
	Y_OFFSET: 10
};

const INFOBOX_DEFAULT_OFFSET:InfoBoxOffset = {
	xo:InfoBox.DEFAULT_X_OFFSET,
	yo:InfoBox.DEFAULT_Y_OFFSET
};

// type AllSatInfo = LiveSatInfo & InfoBoxOffset;

/* sat fill color by type*/
// https://colorbrewer2.org/?type=qualitative&scheme=Set3&n=8
const CELESTRAK_GROUP_COLOR_LIST = ['#eee', '#8dd3c7','#ffffb3','#bebada','#fb8072','#80b1d3','#fdb462','#b3de69','#fccde5'];
const CELESTRAK_GROUP2COLOR = new Map<string, number>();
CELESTRAK_GROUP2COLOR.set("geo", 3);
CELESTRAK_GROUP2COLOR.set("science", 2);
CELESTRAK_GROUP2COLOR.set("gps-ops", 1);
CELESTRAK_GROUP2COLOR.set("galileo", 4);
CELESTRAK_GROUP2COLOR.set("default", 0);

const CELESTRAK_GROUP2NAME = {
	"geo": "Active Geosynchronous",
	"science": "Space & Earth Science",
	"gps-ops": "GPS Operational"
};


const queryString:string = window.location.search;
const urlParams = new URLSearchParams(queryString);

console.log(`url parameters ${urlParams}`);

let id: string = "";
const useWebGl:boolean = true;

// tile pyramid max level (z)
let TP_MAX_Z:number = 8;
// tile size in px
const TP_TILE_SIZE:number = 256;
// total width & height of tile pyramid
let TP_WIDTH:number = TP_TILE_SIZE * Math.pow(2, TP_MAX_Z);
let TP_HEIGHT:number = TP_TILE_SIZE * Math.pow(2, TP_MAX_Z);

let SAT_SYMBOL_SIZE: number = 2;
let SAT_STROKE_SIZE: number = 0.5;
let SAT_TRAJ_STROKE_SIZE: number = 2;

let satMap:Map<string, LiveSatInfo> = new Map<string, LiveSatInfo>(); // Satellites
let orbitMap = new Map<string, Array<Array<Array<number>>>>();
let updateOrbits = false;

// map from norad ID to infobox (when it exists)
let ibMap:Map<string,InfoBoxOffset> = new Map<string,InfoBoxOffset>();

enum SelectionType {
	InfoBox = 1,
	PositionHistory = 2,
}

let selectionType: SelectionType = SelectionType.InfoBox;

let pieMenuRadius: number;
let pieMenuSize: number = 5; //Percent of the height of the wall

let syncServerIp = "127.0.0.1";
if (urlParams.has('syncserver')) {
	syncServerIp = <string>urlParams.get('syncserver');
}

let viewSyncPort = 8080;
if (urlParams.has('viewSyncPort')) {
	viewSyncPort = +<string>urlParams.get('viewSyncPort');
}

let tleUpdatePort = 8084;
if (urlParams.has('tleUpdatePort')) {
	tleUpdatePort = +<string>urlParams.get('tleUpdatePort');
}

let tuioServerIp = "127.0.0.1";
if (urlParams.has('tuioserver')) {
	tuioServerIp = <string>urlParams.get('tuioserver');
}

let tuioPort = 8083;
if (urlParams.has('tuioport')) {
	tuioPort = +<string>urlParams.get('tuioport');
}


let offsetX: number = 1;
if (urlParams.has('offsetx')) {
	// @ts-ignore
	offsetX = +urlParams.get('offsetx');
}
let offsetY: number = 1;
if (urlParams.has('offsety')) {
	// @ts-ignore
	offsetY = +urlParams.get('offsety');
}


let sizeX: number = 800.0;
let sizeY: number = 600.0;

if (urlParams.has('sizex')) {
	// @ts-ignore
	sizeX = +urlParams.get('sizex');
}
if (urlParams.has('sizey')) {
	// @ts-ignore
	sizeY = +urlParams.get('sizey');
}

let wallWidth: number = 800.0;
let wallHeight: number = 600.0;

if (urlParams.has('wallwidth')) {
	// @ts-ignore
	wallWidth = +urlParams.get('wallwidth');
}
if (urlParams.has('wallheight')) {
	// @ts-ignore
	wallHeight = +urlParams.get('wallheight');
}

let dataSourceUrl: string = "http://127.0.0.1:8082";
let tileSourceUrl: string = `${dataSourceUrl}`;
if (urlParams.has('tilesource')) {
	// @ts-ignore
	dataSourceUrl = urlParams.get('tilesource');
	tileSourceUrl = `${dataSourceUrl}`;
}

let tileSourceExt: string = "png";
if (urlParams.has('tileext')) {
	// @ts-ignore
	tileSourceExt = urlParams.get('tileext');
}

let tileFunc = function (z, x, y) {
	return `${tileSourceUrl}/${z}/${x}/${(Math.pow(2,z)-y-1)}.${tileSourceExt}`;
};

if (urlParams.has('satSymbolSize')) {
	let sssStr: string | null = urlParams.get('satSymbolSize');
	if (sssStr) {
		let sss: number = parseFloat(sssStr);
		if (sss && sss > 0) {
			SAT_SYMBOL_SIZE = sss;
			console.log(`Setting satellite symbol size to ${SAT_SYMBOL_SIZE}`);
		} else {
			console.log(`Value for satellite symbol size should be > 0. Received ${sssStr}.`);

		}
	}
}

if (urlParams.has('satStrokeSize')) {
	let sssStr: string | null = urlParams.get('satStrokeSize');
	if (sssStr) {
		let sss: number = parseFloat(sssStr);
		if (sss >= 0) {
			SAT_STROKE_SIZE = sss;
			console.log(`Setting satellite stroke size to ${SAT_STROKE_SIZE}`);
		} else {
			console.log(`Value for satellite stroke size should be >= 0. Received ${sssStr}.`);
		}
	}
}

if (urlParams.has('satPathStrokeSize')) {
	let spssStr: string | null = urlParams.get('satPathStrokeSize');
	if (spssStr) {
		let spss: number = parseFloat(spssStr);
		if (spss >= 0) {
			SAT_TRAJ_STROKE_SIZE = spss;
			console.log(`Setting satellite trajectory stroke size to ${SAT_TRAJ_STROKE_SIZE}`);
		} else {
			console.log(`Value for satellite trajectory stroke size should be >= 0. Received ${spssStr}.`);
		}
	}
}

if (urlParams.has('infoBoxScale')) {
	let ibsStr: string | null = urlParams.get('infoBoxScale');
	if (ibsStr) {
		let ibs: number = parseFloat(ibsStr);
		if (ibs && ibs > 0) {
			InfoBox.SCALE_FACTOR = ibs;
			InfoBox.X_OFFSET = InfoBox.DEFAULT_X_OFFSET * InfoBox.SCALE_FACTOR;
			InfoBox.Y_OFFSET = InfoBox.DEFAULT_Y_OFFSET * InfoBox.SCALE_FACTOR;
			console.log(`Setting infobox size scale to ${InfoBox.SCALE_FACTOR}`);
		} else {
			console.log(`Value for infobox scale factor should be > 0. Received ${ibsStr}.`);

		}
	}
}

const CELESTRAK_GROUP2SHAPE = new Map<string,number[][]>();
CELESTRAK_GROUP2SHAPE.set("geo", WebGlOverlay.generateDiamond(SAT_SYMBOL_SIZE/2.0, 0));
CELESTRAK_GROUP2SHAPE.set("science", WebGlOverlay.generateSquare(SAT_SYMBOL_SIZE, 0));
CELESTRAK_GROUP2SHAPE.set("gps-ops", WebGlOverlay.generateTriangle(SAT_SYMBOL_SIZE, 0));
CELESTRAK_GROUP2SHAPE.set("galileo", WebGlOverlay.generateTriangle(SAT_SYMBOL_SIZE, 0));
const SAT_SHAPE_DEFAULT = WebGlOverlay.generateTriangle(SAT_SYMBOL_SIZE, Math.PI);

let el = document.getElementById("seadragon-viewer");
if (el) {
	el.style.width = `${sizeX}px`;
	el.style.height = `${sizeY}px`;

	console.log(`Canvas size is ${sizeX}px x ${sizeY}px`);
} else {
	el = document.body;
	console.error('Cannot find elem seadragon-viewer');
}

// const socketViewSync: SocketIOClient.Socket = io(`http://${syncServerIp}:${viewSyncPort}`);
const socketTLEUpdate: SocketIOClient.Socket = io(`http://${syncServerIp}:${tleUpdatePort}`);


if (urlParams.has('id')) {
	// @ts-ignore
	id = urlParams.get('id');
} else {
	socketTLEUpdate.emit("message", "ID,ERROR");
}


let wildOsdOptions: WildOsdOptions =
	{
		id: id,
		useWebGl: true,
		osdPyramidLevel: TP_MAX_Z,
		osdTileSize: TP_TILE_SIZE,
		syncServerIp: syncServerIp,
		syncServerPort: viewSyncPort,
		wallWidth: wallWidth,
		wallHeight: wallHeight,
		tileWidth: sizeX,
		tileHeight: sizeY,
		tileOffsetX: offsetX,
		tileOffsetY: offsetY,
		osdTileUrl: tileFunc,
		selectionEnd: selectionEnd,
		tuioServerIp: tuioServerIp,
		tuioServerPort: tuioPort,
		clickDistThreshold: 50,
		dblClickDistThreshold: 50
	};

let wildOsd = new WildOsd("Main", el, wildOsdOptions);

/* get sat color as rgb arrays */
let COLOR_LIST_RGB = new Array<Array<number>>();

for (let color of CELESTRAK_GROUP_COLOR_LIST) {
	COLOR_LIST_RGB.push(new Color(color).unitArray());
}

//PIE MENU
if (wildOsd.isMaster) {
	pieMenuRadius = pieMenuSize * wallHeight / 100 * sizeX / wallWidth;
} else {
	pieMenuRadius = pieMenuSize * wallHeight / 100;
}
let pieMenu = new PieMenu(pieMenuRadius);

wildOsd.osdViewer.addOverlay(pieMenu.root, new Point(0.5, 0.5), Placement.CENTER);

pieMenu.addSlice('#222', 33, "Info", () => {
	if (!wildOsd.isSelectionMode)
		wildOsd.startSelectionMode();
	selectionType = SelectionType.InfoBox;
	pieMenuHide();
});
// pieMenu.addSlice('#222', 25, "Map Nav.", () => {
// 	if (wildOsd.isSelectionMode)
// 		wildOsd.endSelectionMode();
//
// 	pieMenuHide();
// 	wildOsd.overlayRemove(wildOsd.SELECTION_ID);
// 	wildOsd.rpCall("overlayRemove", [wildOsd.SELECTION_ID]);
//
// });
pieMenu.addSlice('#222', 33, "Orbit", () => {
	if (!wildOsd.isSelectionMode)
		wildOsd.startSelectionMode();
	selectionType = SelectionType.PositionHistory;
	pieMenuHide();
});

pieMenu.addSlice('#222', 34, "DragMag", () => {
	console.log("DragMag");
	pieMenuSetLayer("dragmag");
});

let dragMagId = 0;

pieMenu.addSlice('#222', 50, "Remove", () => {
	let x = parseInt(pieMenu.root!.style.left) + pieMenu.r;
	let y = parseInt(pieMenu.root!.style.top) + pieMenu.r;
	wildOsd.removeWindowByPosition(x, y);
	pieMenuSetLayer(PieMenu.pieMenuDefaultLayer);
	pieMenuHide();
}, "dragmag");

pieMenu.addSlice('#222', 50, "Add", () => {
	let x = parseInt(pieMenu.root!.style.left) + pieMenu.r;
	let y = parseInt(pieMenu.root!.style.top) + pieMenu.r;
	let w = 200;
	let h = 200

	let color = randomColor();

	// Add new dragmag before menu div
	let dragMagViewer = wildOsd.addDragMagOsd(pieMenu.root.id, "dragmag" + dragMagId, color, wildOsdOptions, x, y, w, h);


	//Display sats only on this dragmap
	wildOsd.dragMagAddOverlay('dragmag' + dragMagId, satOverlay);

	if (wildOsd.isMaster) {
		//TODO make a helper function in WildOSD
		let xWall = (x * wallWidth) / sizeX;
		let yWall = (y * wallHeight) / sizeY;
		let wWall = (w * wallWidth) / sizeX;
		let hWall = (h * wallHeight) / sizeY;
		console.log("xWall " + xWall);
		wildOsd.rpCall('createDragMagClient', [pieMenu.root.id, "dragmag" + dragMagId, color, xWall, yWall, wWall, hWall]);
	}
	pieMenuSetLayer(PieMenu.pieMenuDefaultLayer);
	pieMenuHide();
	dragMagId++;

}, "dragmag");


function createDragMagClient(rootId: string, id: string, color: string, x: number, y: number, width: number, height: number) {

	// Add new dragmag before menu div
	let dragMagViewer = wildOsd.addDragMagOsd(rootId, id, color, wildOsdOptions, x - wildOsdOptions.tileOffsetX, y - wildOsdOptions.tileOffsetY, width, height);

	//Display sats only on this dragmap
	wildOsd.dragMagAddOverlay(id, satOverlay);

}

wildOsd.rpRegister("createDragMagClient", createDragMagClient);

pieMenu.init();

let overlayRootDiv = pieMenu.root.parentNode;

// SVG OVERLAY (Sat info panels)
let panelsOverlay = new SvgOverlay(wildOsd.osdViewer, overlayRootDiv, "SatOverlay");
d3.select(panelsOverlay.node)
	.append("g")
	.attr("id", "satInfo");


let satOverlay;
if (useWebGl) {
	// WEBGLOVERLAY(satellites and trajectories)
	satOverlay = new WebGlOverlay(wildOsd.osdViewer, {}, overlayRootDiv, COLOR_LIST_RGB);
	satOverlay.setPolygonZoomFactor(4,1);
	satOverlay.lineThickness = 100;
}

const WM_PROJ = d3.geoMercator()
	.center([0, 0])
	.translate([0.5, 0.5])
	.scale(1 / (2 * Math.PI));

socketTLEUpdate.on("Rpc", (funcToCall: Rpc) => {
	let code = funcToCall.func;
	window[code].apply(this, funcToCall.args);
});

const getSatColorByGroup = function(g:string|undefined):number{
	return CELESTRAK_GROUP2COLOR.get(g || 'default') || 0;
}

function updateTLEs(args: string) {
	let satData: [LiveSatInfo] = JSON.parse(args);
	satData.forEach((d) => satMap.set(d.NORAD_CAT_ID, d));
	if (useWebGl) {
		//Create the array to store the sats color index
		let polygonIndex = 0;
		let convexPolygonShapes = new Array<Array<Array<number>>>(satMap.size);
		let convexPolygonPositions = new Array<Array<number>>(satMap.size);
		let convexPolygonColorsIndex = new Array<number>(satMap.size);
		satMap.forEach((d) => {
			const xy = WM_PROJ([d.lon, d.lat]);
			const x = xy[0] * TP_WIDTH;
			const y = xy[1] * TP_HEIGHT;
			convexPolygonColorsIndex[polygonIndex] = getSatColorByGroup(d.group);
			convexPolygonPositions[polygonIndex] = [x, y];
			convexPolygonShapes[polygonIndex++] = CELESTRAK_GROUP2SHAPE.get(d.group || 'default') || SAT_SHAPE_DEFAULT;
			if (ibMap.has(d.NORAD_CAT_ID)){
				adjustInfoBoxTransformSatMove(d.NORAD_CAT_ID, xy);
				d3.select(`#ll${d.NORAD_CAT_ID}`).text(getSatLatLonStr(d));
			}
		});
		satOverlay.setConvexPolygons(convexPolygonShapes, convexPolygonPositions, convexPolygonColorsIndex);
		if (updateOrbits) {
			let chunkCount = 0;
			orbitMap.forEach((positions) => {
				chunkCount += positions.length;
			});
			let colorIndexes = new Array<number>(chunkCount);
			let lines = new Array<Array<Array<number>>>(chunkCount);
			let chunkIndex = 0;
			orbitMap.forEach((positions, norad_id) => {
				positions.forEach((chunk) => {
					lines[chunkIndex] = chunk;
					colorIndexes[chunkIndex] = CELESTRAK_GROUP2COLOR.get(satMap.get(norad_id)!.group||'default')!;
					chunkIndex++;
				});
			});
			satOverlay.setLines(lines, colorIndexes);
			updateOrbits = false;
		}
		wildOsd.osdViewer.forceRedraw();
		console.log(satData.length + " -> " + satMap.size);
	}
}

wildOsd.rpRegister("updateTLEs", updateTLEs);


let scaleTimerId: NodeJS.Timeout | null = null;
// force a scale update first time

wildOsd.osdViewer.addHandler("zoom", () => {
	delayedScaleUpdate();
});


const delayedScaleUpdate = function () {
	if (scaleTimerId)
		clearTimeout(scaleTimerId); //Postpone previous delayedScaleUpdate call
	scaleTimerId = setTimeout(() => {
		updateScaleInvariants();
	}, 500.0);
};
let NAV_SCALE: number = 1;

function updateScaleInvariants() {
	let satsG = document.querySelector("#satInfo");
	if (satsG) {
		let rootG = satsG.parentNode;
		if (rootG) {
			// inspired by https://stackoverflow.com/questions/38224875/replacing-d3-transform-in-d3-v4
			let baseVal = rootG['transform'].baseVal.consolidate();
			let {a, b, c, d, e, f} = baseVal.matrix;
			let scaleX = Math.sqrt(a * a + b * b);
			NAV_SCALE = 1 / scaleX;
			if (NAV_SCALE != 1) {
				// infoboxes
				let sibs = document.querySelectorAll(".satInfoBox");
				for (let sib of sibs) {
					adjustInfoBoxTransformScale(sib);
				}
			}
		}
	}
}

function adjustInfoBoxTransformSatMove(satId:string, xy:number[]):any {
	let ib:InfoBoxOffset = ibMap.get(satId) || INFOBOX_DEFAULT_OFFSET;
	let dx: number = xy[0] + ib.xo * NAV_SCALE * InfoBox.SCALE_FACTOR;
	let dy: number = xy[1] + ib.yo * NAV_SCALE * InfoBox.SCALE_FACTOR;
	return d3.select(`g#${InfoBox.ID_PREFIX}${satId}`)
	  .attr('transform',
	  	    `translate(${dx},${dy}) scale(${NAV_SCALE * InfoBox.SCALE_FACTOR})`);
}

function adjustInfoBoxTransformScale(el: Element) {
	let satData: LiveSatInfo = d3.select(el).datum();
	let xy = WM_PROJ([satData.lon, satData.lat]);
	let ib = ibMap.get(satData.NORAD_CAT_ID) || INFOBOX_DEFAULT_OFFSET;
	let dx: number = xy[0] + ib.xo * NAV_SCALE * InfoBox.SCALE_FACTOR;
	let dy: number = xy[1] + ib.yo * NAV_SCALE * InfoBox.SCALE_FACTOR;
	el.setAttribute('transform',
		`translate(${dx},${dy}) scale(${NAV_SCALE * InfoBox.SCALE_FACTOR})`);
	adjustInfoBoxLinkGeom(el, dx, dy, false);
}

function getInfoBoxTransformFromSatPos(ll: number[]): string {
	let xy = WM_PROJ(ll);
	return `translate(${xy[0] + InfoBox.X_OFFSET * NAV_SCALE * InfoBox.SCALE_FACTOR},${xy[1] + InfoBox.Y_OFFSET * NAV_SCALE * InfoBox.SCALE_FACTOR}) scale(${NAV_SCALE * InfoBox.SCALE_FACTOR})`;
}

// input: infobox <g> ID,
// x,y are the translate parameters of that <g>'s transform
function adjustInfoBoxLinkGeom(ibGEl: Element, x: number, y: number,
							   updateOffset: boolean) {
	let ibG = d3.select(ibGEl);
	let satData: LiveSatInfo = ibG.datum();
	let xy = WM_PROJ([satData.lon, satData.lat]);
	if (updateOffset) {
		// infobox is being dragged, store custom offset to sat
		// which we want to be scale invariant
		let ib:InfoBoxOffset = ibMap.get(satData.NORAD_CAT_ID) || INFOBOX_DEFAULT_OFFSET;
		ib.xo = (x - xy[0]) / (NAV_SCALE * InfoBox.SCALE_FACTOR);
		ib.yo = (y - xy[1]) / (NAV_SCALE * InfoBox.SCALE_FACTOR);
		ibMap.set(satData.NORAD_CAT_ID, ib);
		ibG.select("line")
			.attr("x2", -ib.xo)
			.attr("y2", -ib.yo);
	} else {
		// infobox is merely being adjusted to remain scale invariant as
		// we zoom in/out. The custom offset should not be updated.
		ibG.select("line")
			.attr("x2", (xy[0] - x) / (NAV_SCALE * InfoBox.SCALE_FACTOR))
			.attr("y2", (xy[1] - y) / (NAV_SCALE * InfoBox.SCALE_FACTOR));
	}
}

function requestDiscardInfoBox(satId: string) {
	if (satId && wildOsd.isMaster) {
		wildOsd.rpCall("broadcastDiscardInfoBox", [satId], socketTLEUpdate);
	}
}

function discardInfoBox(satId: string) {
	d3.select(`g#${InfoBox.ID_PREFIX}${satId}`).remove();
	if (ibMap.has(satId)){
		ibMap.delete(satId);
	}
}

wildOsd.rpRegister("discardInfoBox", discardInfoBox);


function removeDiv(divId: string) {
	//console.log("removeDiv "+divId);
	let divToDestroy = document.getElementById(divId);
	if (divToDestroy) {
		divToDestroy.remove();
	}
}

wildOsd.rpRegister("removeDiv", removeDiv);

let checkSatClick = false;
let lastClickPos: Point = new Point();

wildOsd.osdViewer.addHandler('canvas-click', function (viewerEvent) {
	if (viewerEvent.position) {
		let elements = document.elementsFromPoint(viewerEvent.position.x, viewerEvent.position.y);
		for (let i = 0; i < elements.length; i++) {
			let element = elements[i];
			if (element.getAttribute("class") == PieMenu.pieSliceClassName) {
				pieMenu.click(element.id);
				return;
			}
		}
		if (viewerEvent.quick) {
			//Click and not drag
			checkSatClick = true;
			lastClickPos = viewerEvent.position;
			wildOsd.osdViewer.forceRedraw();
		}
	}
});

wildOsd.osdViewer.addHandler('canvas-key', function (viewerEvent) {
	if (viewerEvent['originalEvent']['type'] == 'keypress') {
		if (viewerEvent['originalEvent']['key'] == 'm') {
			if (wildOsd.isSelectionMode) {
				wildOsd.endSelectionMode()
			} else {
				selectionType = SelectionType.InfoBox;
				wildOsd.startSelectionMode();
			}
		}
	}
});

//TODO move it in WildOsd
function pieMenuShow(x: number, y: number) {
	let pieMenuOverlay: OpenSeadragon.Overlay = wildOsd.osdViewer.getOverlayById(PieMenu.pieMenuIdName);

	if (pieMenuOverlay) {
		pieMenuOverlay.update(new Point(x, y), Placement.CENTER);
		pieMenu.show();
		wildOsd.osdViewer.forceRedraw()
	}
	if (wildOsd.isMaster)
		wildOsd.rpCall("pieMenuShow", [x, y]);

}

wildOsd.rpRegister("pieMenuShow", pieMenuShow);

function pieMenuHide() {
	pieMenu.hide();
	wildOsd.osdViewer.forceRedraw()
	if (wildOsd.isMaster)
		wildOsd.rpCall("pieMenuHide", []);
}

wildOsd.rpRegister("pieMenuHide", pieMenuHide);

function pieMenuSetLayer(layerID: string) {
	pieMenu.setCurrentLayer(layerID);
	wildOsd.osdViewer.forceRedraw()
	if (wildOsd.isMaster)
		wildOsd.rpCall("pieMenuSetLayer", [layerID]);
}

wildOsd.rpRegister("pieMenuSetLayer", pieMenuSetLayer);


wildOsd.osdViewer.addHandler('canvas-double-click', function (viewerEvent) {
	if (viewerEvent.position) {
		let viewportPos = wildOsd.osdViewer.viewport.viewerElementToViewportCoordinates(viewerEvent.position);
		pieMenuShow(viewportPos.x, viewportPos.y);
	}
})

function translateInfoBox(svgGElement: SVGGElement, x: number, y: number) {
	//@ts-ignore
	let matrix = svgGElement.transform.baseVal.consolidate().matrix;
	let {a, b, c, d, e, f} = matrix;
	let scaleX = Math.sqrt(a * a + b * b);
	// let scaleY = Math.sqrt(c * c + d * d);
	svgGElement.setAttribute("transform", `translate(${e + x},${f + y}) scale(${scaleX})`);
	adjustInfoBoxLinkGeom(svgGElement, e, f, true);
}

function translateInfoBoxfromId(id: string, x: number, y: number) {
	translateInfoBox(document.getElementById(id) as unknown as SVGGElement, x, y);
}

wildOsd.rpRegister("translateInfoBoxfromId", translateInfoBoxfromId);

function requestSatDetailDisplay(d: LiveSatInfo | null) {
	if (d) {
		wildOsd.rpCall("broadcastSatDetailDisplay", [d.NORAD_CAT_ID], socketTLEUpdate);
	}
}

function requestSatDetailDisplayByCoordinates(geoTopLeft: Point, geoBottomRight: Point) {
	satMap.forEach((d) => {
		if (d.lon <= geoBottomRight[0] &&
			d.lon >= geoTopLeft[0] &&
			d.lat <= geoTopLeft[1] &&
			d.lat >= geoBottomRight[1]) {
			requestSatDetailDisplay(d);
		}
	});
}

function requestOrbitDisplayByCoordinates(geoTopLeft: Point, geoBottomRight: Point) {
	satMap.forEach((d) => {
		if (d.lon <= geoBottomRight[0] &&
			d.lon >= geoTopLeft[0] &&
			d.lat <= geoTopLeft[1] &&
			d.lat >= geoBottomRight[1]) {
			wildOsd.rpCall("broadcastOrbitDisplay", [d.NORAD_CAT_ID], socketTLEUpdate);
		}
	});
};

function selectionEnd(imageTopLeft: Point, imagebottomRight: Point) {
	//Get total image size to normalize
	let imageSize = wildOsd.osdViewer.world.getItemAt(0).getContentSize();

	let geoTopLeft = WM_PROJ.invert([imageTopLeft.x / imageSize.x, imageTopLeft.y / imageSize.y]);
	let geoBottomRight = WM_PROJ.invert([imagebottomRight.x / imageSize.x, imagebottomRight.y / imageSize.y]);

	//console.log(`Selection geo ${geoTopLeft} -> ${geoBottomRight}` );

	switch (selectionType) {
		case SelectionType.InfoBox:
			if (wildOsd.isMaster) {
				requestSatDetailDisplayByCoordinates(geoTopLeft, geoBottomRight);
			}
			break;
		case SelectionType.PositionHistory:
			if (wildOsd.isMaster) {
				requestOrbitDisplayByCoordinates(geoTopLeft, geoBottomRight);
			}
			break;
		default:
			console.log("Unknown selectionType");
	}

	wildOsd.endSelectionMode();

	wildOsd.overlayRemove(wildOsd.SELECTION_ID);
	wildOsd.rpCall("overlayRemove", [wildOsd.SELECTION_ID]);
}

function displaySatDetails(args: string) {
	let selectedSat: LiveSatInfo = JSON.parse(args);
	let ib:InfoBoxOffset = {
		xo: InfoBox.X_OFFSET,
		yo: InfoBox.Y_OFFSET
	};
	ibMap.set(selectedSat.NORAD_CAT_ID, ib);
	let infoBoxG = d3.select("#satInfo").selectAll(`g#${InfoBox.ID_PREFIX}${selectedSat.NORAD_CAT_ID}`)
		.data([selectedSat])
		.enter()
		.append("g")
		.attr("id", `${InfoBox.ID_PREFIX}${selectedSat.NORAD_CAT_ID}`)
		.attr("transform", getInfoBoxTransformFromSatPos([selectedSat.lon, selectedSat.lat]))
		.classed("satInfoBox", true);

	//Drag & drop
	// @ts-ignore
	let dragElement: SVGGElement = document.getElementById(`${InfoBox.ID_PREFIX}${selectedSat.NORAD_CAT_ID}`);
	new OpenSeadragon.MouseTracker({
		element: dragElement,
		preProcessEventHandler: function (eventInfo) {
			switch (eventInfo.eventType) {
				case 'pointerdown':
				case 'pointerup':
					// prevent pointerdown/pointerup events from bubbling
					// viewer won't see these events
					eventInfo.stopPropagation = true;
			}
		}, dragHandler: function (e) {
			// @ts-ignore
			let delta = wildOsd.osdViewer.viewport.deltaPointsFromPixels(e.delta);
			translateInfoBox(dragElement, delta.x, delta.y);
			// broadcast translate
			wildOsd.rpCall("translateInfoBoxfromId",
				[dragElement.id, delta.x, delta.y]);
		}
	});

	let colorIndex:number = getSatColorByGroup(selectedSat.group);

	// link to sat glyph
	infoBoxG.data([selectedSat])
		.append("line")
		.attr("x1", (d) => (InfoBox.WIDTH / 2))
		.attr("y1", (d) => (InfoBox.HEIGHT / 2))
		.attr("x2", (d) => (-InfoBox.X_OFFSET))
		.attr("y2", (d) => (-InfoBox.Y_OFFSET))
		.attr('stroke-width', `${SAT_STROKE_SIZE}px`)
		.attr("class", (d) => (`IB${colorIndex}ds`));
	// infobox background
	infoBoxG.append("rect")
		.attr("x", 0)
		.attr("y", 0)
		.attr("width", InfoBox.WIDTH)
		.attr("height", InfoBox.HEIGHT)
		.attr("class", (d) => (`IB${colorIndex}lf IB${colorIndex}ds`));
	// title [op status] owner
	infoBoxG.append("text")
		.attr("x", InfoBox.MARGIN_LEFT)
		.attr("y", 10)
		.attr("text-anchor", "start")
		.attr("class", "title")
		.attr("pointer-events", "none")
		.text((d) => (d.OBJECT_NAME));
	infoBoxG.append("text")
		.attr("x", InfoBox.WIDTH - InfoBox.MARGIN_RIGHT - 12)
		.attr("y", 10)
		.attr("text-anchor", "end")
		.attr("pointer-events", "none")
		.text((d) => (`[${d.OPS_STATUS_CODE}] ${d.OWNER}`));
	// NORAD & International Designator
	infoBoxG.append("text")
		.attr("x", InfoBox.MARGIN_LEFT)
		.attr("y", 20)
		.attr("class", (d) => (`IB${colorIndex}df`))
		.attr("pointer-events", "none")
		.text("NORAD");
	infoBoxG.append("text")
		.attr("x", InfoBox.MARGIN_LEFT + 28)
		.attr("y", 20)
		.attr("pointer-events", "none")
		.text((d) => (d.NORAD_CAT_ID));
	infoBoxG.append("text")
		.attr("x", InfoBox.MARGIN_LEFT + 70)
		.attr("y", 20)
		.attr("class", (d) => (`IB${colorIndex}df`))
		.attr("pointer-events", "none")
		.text("ID");
	infoBoxG.append("text")
		.attr("x", InfoBox.MARGIN_LEFT + 85)
		.attr("y", 20)
		.attr("pointer-events", "none")
		.text((d) => (d.OBJECT_ID));
	// launch date & site, decay date
	infoBoxG.append("text")
		.attr("x", InfoBox.MARGIN_LEFT)
		.attr("y", 30)
		.attr("pointer-events", "none")
		.text((d) => (getSatLaunchStr(d)));
	infoBoxG.append("text")
		.attr("x", InfoBox.WIDTH - InfoBox.MARGIN_RIGHT)
		.attr("y", 30)
		.attr("text-anchor", "end")
		.attr("pointer-events", "none")
		.text((d) => (getSatDecayStr(d)));
	// lat/lon, period, apogee/perigee
	infoBoxG.append("text")
		.attr("id", (d) => (`ll${d.NORAD_CAT_ID}`))
		.attr("x", InfoBox.MARGIN_LEFT)
		.attr("y", 40)
		.attr("pointer-events", "none")
		.text((d) => (getSatLatLonStr(d)));
	infoBoxG.append("text")
		.attr("x", InfoBox.WIDTH / 2)
		.attr("y", 40)
		.attr("text-anchor", "middle")
		.attr("pointer-events", "none")
		.text((d) => (getSatPeriodStr(d)));
	infoBoxG.append("text")
		.attr("x", InfoBox.WIDTH - InfoBox.MARGIN_RIGHT)
		.attr("y", 40)
		.attr("text-anchor", "end")
		.attr("pointer-events", "none")
		.text((d) => (getSatApogeePerigeeStr(d)));
	// // ship image
	// infoBoxG.append("image")
	// 	.attr("x", (InfoBox.WIDTH - InfoBox.SHIP_THUMB_WIDTH) / 2)
	// 	.attr("y", 45)
	// 	.attr("id", `img${selectedShip.id}`)
	// 	.attr("xlink:href", (d) => (`${shipImgSourceUrl}/${d.imgUrl}`))
	// 	.attr("width", InfoBox.SHIP_THUMB_WIDTH);
	// // close icon
	infoBoxG.append("image")
		.attr("x", InfoBox.WIDTH - 18)
		.attr("y", 3)
		.attr("xlink:href", "images/close@2x.png")
		.attr("width", 15).attr("height", 15)
		.on("pointerdown", (event, d) => (requestDiscardInfoBox(d.NORAD_CAT_ID)));
}

wildOsd.rpRegister("displaySatDetails", displaySatDetails);

function getSatLatLonStr(satData: LiveSatInfo): string {
	let res: string = (satData.lat < 0) ? `${-satData.lat.toFixed(3)} S` : `${satData.lat.toFixed(3)} N`;
	return res + ((satData.lon < 0) ? ` ${-satData.lon.toFixed(3)} W` : ` ${satData.lon.toFixed(3)} E`);
}

function getSatLaunchStr(satData: LiveSatInfo): string {
	return `Launch: ${satData.LAUNCH_DATE} (${satData.LAUNCH_SITE})`;
}

function getSatDecayStr(satData: LiveSatInfo): string {
	if (satData.DECAY_DATE){
		return `Decay: ${satData.DECAY_DATE}`;
	}
	else {
		return "";
	}
}

function getSatPeriodStr(satData: LiveSatInfo): string {
	return `${Number.parseFloat(satData.PERIOD).toFixed(0)}min`;
}

function getSatApogeePerigeeStr(satData: LiveSatInfo): string {
	return `${satData.APOGEE}km/${satData.PERIGEE}km`;
}

function displayOrbit(args:string){
	let orbitData = JSON.parse(args);
	let orbitChunks:number[][][] = orbitData['orbchunks'];
	if (orbitChunks.length <= 0) return;
	let positions = new Array<Array<Array<number>>>(orbitChunks.length);
	for (let j=0;j<orbitChunks.length;j++){
		let positionSeq = new Array<Array<number>>(orbitChunks[j].length);
		for (let i = 0; i < orbitChunks[j].length; i++) {
			let xy = WM_PROJ([orbitChunks[j][i][0], orbitChunks[j][i][1]]);
			let x = xy[0] * TP_WIDTH;
			let y = xy[1] * TP_HEIGHT;
			positionSeq[i] = [x, y];
		}
		positions[j] = positionSeq;
	}
	orbitMap.set(orbitData['id'], positions);
	updateOrbits = true;
}

wildOsd.rpRegister("displayOrbit", displayOrbit);
