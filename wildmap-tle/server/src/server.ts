const express = require('express');
const fs = require('fs');
const https = require('https');
import * as socketio from "socket.io";
import * as path from "path";
const chalk = require('chalk');
const csv = require('csv-parser');
const yargs = require('yargs/yargs');
const { hideBin } = require('yargs/helpers');
const argv = yargs(hideBin(process.argv)).argv;
const spacetrack = require('spacetrack');
const satellite = require('satellite.js');

const CELESTRAK_QUERY_GROUPS:string[] = [
	"geo",
	"science",
	"gps-ops",
	"galileo"
];

enum LogType {
	PLAIN = 0,
	INFO = 1,
	WARNING = 2,
	ERROR = 3
}

/*------------------------------------------*/
/* Logging                                  */
/*------------------------------------------*/

const log = function(msg:string, tl:number, ltype?:LogType){
	if (tl <= TRACE_LEVEL){
		if (typeof(ltype) !== 'undefined'){
			switch (ltype) {
				case LogType.INFO:{console.log(`INFO: ${chalk.blue(msg)}`);break;}
				case LogType.WARNING:{console.log(`WARNING: ${chalk.orange(msg)}`);break;}
				case LogType.ERROR:{console.log(`ERROR: ${chalk.red(msg)}`);break;}
				case LogType.PLAIN:{console.log(msg);break;}
			}
		}
		else {
			console.log(msg);
		}
	}
}

const tleUpdatePort = 8084;

interface SATCATInfo {
	OBJECT_NAME:string;
	OBJECT_ID:string;
	NORAD_CAT_ID:string;
	OBJECT_TYPE:string;
	OPS_STATUS_CODE:string;
	OWNER:string;
	LAUNCH_DATE:string;
	LAUNCH_SITE:string;
	DECAY_DATE:string;
	PERIOD:string;
	INCLINATION:string;
	APOGEE:string;
	PERIGEE:string;
	RCS:string;
	DATA_STATUS_CODE:string;
	ORBIT_CENTER:string;
	ORBIT_TYPE:string;
};

interface SatPosInfo {
	lat?:number;
	lon?:number;
	group?:string;
	satrec?:Object;
};

type LiveSatInfo = SATCATInfo & SatPosInfo;

interface wTLE {
	l1:string;
	l2:string;
	group:string;
	name:string;
};

// https://celestrak.com/NORAD/elements/
//const TLE_URL = "https://celestrak.com/NORAD/elements/gp.php?GROUP=science&FORMAT=tle";
const UPDATE_FREQ = 2000; // in milliseconds

// norad ID to TLE + Celestrak group + name
const currentTLEs = new Map<string, wTLE>();

let SATCAT_DATA_DIR = "data";
if (argv.satcat){
	SATCAT_DATA_DIR = argv.satcat;
}
const SATCAT_CSV = `${SATCAT_DATA_DIR}/satcat.csv`;
const CSV_DELIMITER:string = ",";
// key is the NORAD catalog number, which can also be found in the TLE (l1 & l2)
const SATCAT = new Map<string,SATCATInfo>();

let TRACE_LEVEL:number = 0;
if (argv.tl){
	TRACE_LEVEL = argv.tl;
}

const loadSATCAT = function(){
	// parse local static data
	fs.createReadStream(SATCAT_CSV)
	  .on('error', (err:any) => {log(err, 0, LogType.ERROR);})
      .pipe(csv({ separator: CSV_DELIMITER }))
      .on('data', (data:SATCATInfo) => {addSATCATEntry(data);})
      .on('end', () => {startTLEUpdater();});
};

const addSATCATEntry = function(data:SATCATInfo){
	SATCAT.set(data.NORAD_CAT_ID, data);
}

const app = express();

let tleUpdateServer = app.listen(tleUpdatePort, "0.0.0.0", () => {
	log(`TLE server listening on ${tleUpdatePort}`, 2, LogType.INFO);
});


app.get("/", (req: any, res: any) => {
	res.sendFile(path.resolve("./dist/index.html"));
});

let tleUpdateSocket = socketio.listen(tleUpdateServer);
// let tleUpdateSocket = require('socket.io').listen(tleUpdateServer);

tleUpdateSocket.on("connect", (socket: socketio.Socket) => {
	socket.on("ID", (message: any) => {
		if (message === "MASTER") {
			log("Master listening for viewport updates", 2, LogType.INFO);
		} else {
			log(`Slave ${message} listening for viewport updates`, 2, LogType.INFO);
		}
	});

	socket.on("Rpc", (message: any) => {
		if (message['func'] == "broadcastSatDetailDisplay"){
			broadcastSatDetailDisplay(message.args);
		}
		else if (message['func'] == "broadcastOrbitDisplay"){
			broadcastOrbitDisplay(message.args);
		}
		else if (message['func'] == "broadcastDiscardInfoBox"){
			broadcastDiscardInfoBox(message.args);
		}
		else {
			// directly forward viewport transform commands to all clients
			 // console.log("Rpc -> "+JSON.stringify(message));
			//Forward message to slaves
			socket.broadcast.emit("Rpc",message);
		}

	});
});
//
// // XXX seems to be overriding .on() call above
// tleUpdateSocket.on("connect", (socket: socketio.Socket) => {
// 	socket.on("ID", (message: any) => {
// 		if (message === "MASTER") {
// 			log("Master listening for TLE updates", 2, LogType.INFO);
// 		} else {
// 			log(`Slave ${message} listening for TLE updates`, 2, LogType.INFO);
// 		}
// 	});
// });

let broadcastOrbitDisplay = function(args:any[]){
	const norad_id:string = args[0];
	if (norad_id){
		computeOrbit(norad_id);
	}
};

const computeOrbit = function(norad_id:string){
	log(`Display orbit for ${norad_id}`, 2, LogType.INFO);
	let orbitChunks:number[][][] = [];
	let date = new Date();
	var gmst = satellite.gstime(date);
	let tle = currentTLEs.get(norad_id);
	let satrec = satellite.twoline2satrec(tle.l1, tle.l2);
	let positionEci = satellite.propagate(satrec, date).position;
	let positionGd = satellite.eciToGeodetic(positionEci, gmst);
	let p_lon = satellite.degreesLong(positionGd.longitude);
	let p_lat = satellite.degreesLat(positionGd.latitude);
	let i = 0;
	let orbitChunk:number[][] = [];
	while(i < 120){// x 2min
		gmst = satellite.gstime(date);
		positionEci = satellite.propagate(satrec, date).position;
		positionGd = satellite.eciToGeodetic(positionEci, gmst);
		let lon = satellite.degreesLong(positionGd.longitude);
		let lat = satellite.degreesLat(positionGd.latitude);
		if (Math.abs(lat) > 85.051129){ // boundary latitude of slippymap square
			if (orbitChunk.length > 1){
				// add chunk only if it contains more than one point
				orbitChunks.push(orbitChunk);
			}
			orbitChunk = [];
		}
		else if (Math.abs(lon-p_lon) > 100 || Math.abs(lat-p_lat) > 50){
			if (orbitChunk.length > 1){
				// add chunk only if it contains more than one point
				orbitChunks.push(orbitChunk);
			}
			orbitChunk = [[lon,lat]];
		}
		else {
			orbitChunk.push([lon, lat]);
		}
		p_lon = lon;
		p_lat = lat;
		date.setMinutes(date.getMinutes() + 2);
		i++;
	}
	orbitChunks.push(orbitChunk);
	sendOrbitForDisplay(norad_id, orbitChunks);
};

const updateTLEs = function(){
	let date = new Date();
	log(`Updating satellites at ${date.toString()}`, 2, LogType.INFO);
	var gmst = satellite.gstime(date);
	let sats:LiveSatInfo[] = [];
	for (let sat of currentTLEs.entries()){
		let norad_id = sat[1].l2.split(/\s+/)[1];
		if (norad_id){
			let satData:LiveSatInfo = SATCAT.get(norad_id);
			if (satData){
				satData.satrec = satellite.twoline2satrec(sat[1].l1, sat[1].l2);
				let positionEci = satellite.propagate(satData.satrec, date).position;
				let positionGd = satellite.eciToGeodetic(positionEci, gmst);
				let lon = satellite.degreesLong(positionGd.longitude)
				let lat = satellite.degreesLat(positionGd.latitude)
				satData.lat = lat;
				satData.lon = lon;
				satData.group = sat[1].group;
				sats.push(satData);
			}
			else {
				log(`No catalog entry for NORAD ID ${norad_id}`, 1, LogType.WARNING);
			}
		}
		else {
			log(`Failed to parse TLE.l2 ${sat[1].l2}`, 1, LogType.WARNING);
		}
	}
	log(`Updated ${sats.length} satellites`, 2, LogType.INFO);
	sendTLEUpdates(sats);
};

const sendTLEUpdates = function(sats:LiveSatInfo[]){
	let jsonTLEs = JSON.stringify(sats);
	tleUpdateSocket.emit("Rpc",
		{func:"updateTLEs", args:[jsonTLEs]});
};

const parseTLEs = function(tle:string, group:string){
	let lines:string[] = tle.split('\n');
	let name:string = null;
	let norad_id:string = null;
	for (let line of lines){
		let tline:string = line.trim();
		if (tline.startsWith("1 ")){
			norad_id = tline.split(/\s+/)[1].slice(0,-1);
			currentTLEs.set(norad_id, {l1:tline,l2:null,group:group,name:name});
		}
		else if (tline.startsWith("2 ")){
			currentTLEs.get(norad_id).l2 = tline;
		}
		else {
			name = tline;
		}
	}
}

const fetchTLEs = function(group:string, lastGroup:boolean){
	const TLE_URL = `https://celestrak.com/NORAD/elements/gp.php?GROUP=${group}&FORMAT=tle`;
	log(`Fetching TLEs from ${TLE_URL}`, 2, LogType.INFO);
	https.get(TLE_URL, (response:any) => {
		let data = '';
	  	response.on("data", (chunk:any) => {
			data += chunk.toString();
	  	});
		response.on('end', () => {
    		parseTLEs(data, group);
			if (lastGroup){
				log("Starting updater", 1);
				setInterval(updateTLEs, UPDATE_FREQ);
			}
  		});
  	}).on('error', function(e:any) {
	  	log("Error message : " + e.message, 0, LogType.ERROR);
	});
}


// given the low frequency of TLE updates (a few times a day maximum in most cases)
// in this demo TLEs are only fetched once at launch time
// sat position is updated locally using computations performed by satellite.js
const startTLEUpdater = function(){
	for (let i:number=0;i<CELESTRAK_QUERY_GROUPS.length;i++){
		fetchTLEs(CELESTRAK_QUERY_GROUPS[i], i==(CELESTRAK_QUERY_GROUPS.length-1));
	}
};

const stopTLEUpdater = function(){

};

loadSATCAT();

const sendOrbitForDisplay = function(norad_id:string, orbit:number[][][]){
	let res = {'id': norad_id, 'orbchunks': orbit};
	let jsonOrbit = JSON.stringify(res);
	tleUpdateSocket.emit("Rpc",
		{func:"displayOrbit", args:[jsonOrbit]});
};

const broadcastSatDetailDisplay = function(args:any[]){
	const norad_id:string = args[0];
	if (norad_id){
		let sci:SATCATInfo = SATCAT.get(norad_id);
		if (sci){
			log(`Dispatching request to show info about ${norad_id}`, 2, LogType.INFO);
			sendSatDetailsForDisplay(sci);
		}
	}
};

const sendSatDetailsForDisplay = function(sat:SATCATInfo){
	let jsonSat = JSON.stringify(sat);
	tleUpdateSocket.emit("Rpc",
			{func:"displaySatDetails", args:[jsonSat]});
};


const broadcastDiscardInfoBox = function(norad_id:string){
	if (norad_id){
		log(`Dispatching request to hide info about ${norad_id}`, 2, LogType.INFO);
		tleUpdateSocket.emit("Rpc",
			{func:"discardInfoBox", args:[norad_id]});
	}
};
