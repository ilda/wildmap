
offsetX=0
offsetY=0
wrow=5
wtile_w=14400
wtile_h=960
viewSyncPort=8080
serverport=8081
tileport=8082
tuioport=8083
shipUpdatePort=8084

electronpath="electron"
electronstartscript="electronmain.js"
electronpreloadscript="preload.js"

user=wild
wallwidth=14400
wallheight=4800
#proxyaddress="192.168.2.254:8086"

#master canvas size. MUST be the same ratio than the wall
masterwidth=1440
masterheight=480


packagefile="package.json"
nodetmppath="/home/wild/tmp/wildmap"
nodeuserdirpath="/home/wild/.config/wildmap"

servertmppath="/home/wild/tmp/wildmap/"
serverpackagepath=$servertmppath$packagefile
echo $serverpackagepath

serverip="192.168.2.121"
tileserverip="127.0.0.121"

clientcachedir="/home/wild/cache/wildmap"
electronstartscript="electronmain.js"
electronpreloadscript="preload.js"
shipSymbolSize=2
shipSymbolSizeMaster=2

shipStrokeSize=2
shipPathStrokeSize=3
infoBoxScale=1
infoBoxScaleMaster="0.1"


jitsiCertificate="-----BEGIN CERTIFICATE-----
                 MIIDKDCCAhCgAwIBAgIURtRwnoavj2iBbqYAv13Y+E2TKmowDQYJKoZIhvcNAQEL
                 BQAwFzEVMBMGA1UEAwwMMTkyLjE2OC4yLjM1MB4XDTIzMDIwOTE1MzEyOVoXDTI0
                 MDIwOTE1MzEyOVowFzEVMBMGA1UEAwwMMTkyLjE2OC4yLjM1MIIBIjANBgkqhkiG
                 9w0BAQEFAAOCAQ8AMIIBCgKCAQEAyFUt3mE5g0xHDjSZbxl2UqON0Nsdc4jMLHcT
                 l1TF5C54PuvQzsQVlnRGJMiV0tQOWmgy+LuZTEu48k3Uaf8tPc6vlgzr2cMU8nRs
                 UZPsv0jn+yO1RCRMZZWPYUEn62tR1tKVrgmSpGhDgtkPfVBz5V7tQSEemTPUXqJ+
                 OlCN/k+Uq7idL42sZoN6v4zKf/qAHn9m/yIYV53KIeqfudAOdv5KGKIeX6SwrwF4
                 mLnQWf5kRnqgU4u+GX4bgqoNNnt44147+ZKJ8tc+SUwH8y4u9kiijt1bHDepX019
                 TL1CfVgw+InjrPw1NuRDkwT/fYOQHY0jhIIVU+tCNiQUhkvh7QIDAQABo2wwajAL
                 BgNVHQ8EBAMCBeAwEwYDVR0lBAwwCgYIKwYBBQUHAwEwJwYDVR0RBCAwHoIJaG9t
                 ZS5hcnBhggsqLmhvbWUuYXJwYYcEwKgCIzAdBgNVHQ4EFgQUfAtBde3iuKaFH2uP
                 ksauBzUsnE4wDQYJKoZIhvcNAQELBQADggEBAJyAxiRLBes1Q+/mX9kmmOesGWxL
                 xDJQ1f67Xlo1A/kk7Y7Uq02m5V9dHOkb1nlO3witlkbVDziWvKIxQB9XEci5a/VW
                 u0ssrIrxoDDlrfljUiBcl4P5goJ955WkqB8ufQcfyeIaQ9tvqcBsSHTjg6uE0rGJ
                 m2ashlZ94nQvg2d1S5jp8SyvipMgRK3fMkTp/LpkLl0EdwjHIuMGwZP7NtsfTuX3
                 91nKpSYf46LP2/rNyDd8UCKJfWKsr+J3URQ7zWvkBqHnEiFHFls0SfR6eJiweB8I
                 f4dqmzGUCFAhk5AsYuY1Kvc6d+GZMEePV3iGbKORax9PhM/rFMr5l/wRpwI=
                 -----END CERTIFICATE-----"




# echo "clear nw cache"

# # walldo rm -r $nodeuserdirpath
#  #walldo rm -r $nodetmppath
#  walldo rm -r $clientcachedir
#  sleep 1
#  #walldo mkdir $nodetmppath
#  walldo mkdir $clientcachedir

# sleep 1

echo "start nw on nodes"
for row in {1..5}
  do
    echo  {\"name\": \"wildmap\",\"main\": \"$nodetmppath/electronmain.js\",\"launch\": "\"http://$serverip:$serverport/?id=c$row&offsetx=$offsetX&offsety=$offsetY&wallx=$wcol&wally=$wrow&wallwidth=$wallwidth&wallheight=$wallheight&sizex=$wtile_w&sizey=$wtile_h&tilesource=http://$tileserverip:$tileport&syncserver=$serverip&viewSyncPort=$viewSyncPort&shipUpdatePort=$shipUpdatePort&ESRI=$esri&esriaddress=$esriaddress&shipSymbolSize=$shipSymbolSize&shipStrokeSize=$shipStrokeSize&shipPathStrokeSize=$shipPathStrokeSize&infoBoxScale=$infoBoxScale\"",\"window\": {\"kiosk\": false,\"fullscreen\": false,\"width\": $wtile_w,\"height\": $wtile_h},\"additional_trust_anchors\": \"$jitsiCertificate\"} > $serverpackagepath$row
    offsetY=$((offsetY+wtile_h))
  done

#NG
#echo  {\"name\": \"wildmap\",\"nodejs\": \"true\",\"main\": "\"http://$serverip:$serverport/?id=c1&offsetx=0&offsety=3840&wallx=1&wally=5&wallwidth=$wallwidth&wallheight=$wallheight&sizex=14400&sizey=960&tilesource=http://#$tileserverip:$tileport&syncserver=$serverip&viewSyncPort=$viewSyncPort&shipUpdatePort=$shipUpdatePort&ESRI=$esri&esriaddress=$esriaddress&shipSymbolSize=$shipSymbolSize&shipStrokeSize=$shipStrokeSize&shipPathStrokeSize=$shipPathStrokeSize&infoBoxScale=$infoBoxScale\"",\"window\": {\"kiosk\": true,\"fullscreen\": true,\"width\": 14400,\"height\": 960},\"additional_trust_anchors\": \"$jitsiCertificate\",\"chromium-args\": \"--enable-file-cookies --allow-running-insecure-content --ignore-#certificate-errors\"} > $serverpackagepath



for row in {1..5}
  do
    scp $electronstartscript $user@c$row:$nodetmppath/
    scp $electronpreloadscript $user@c$row:$nodetmppath/
    scp $serverpackagepath$row $user@c$row:$nodetmppath/$packagefile
  done


for row in {1..5}
  do
    ssh $user@c$row "cd $nodetmppath ; DISPLAY=:0 $electronpath . "&
  done


#NG
#echo $user@192.168.2.51 DISPLAY=:0 $nwpath $nodetmppath
#ssh $user@192.168.2.51 DISPLAY=:0 $nwpath $nodetmppath &


sleep 2


#echo  {\"name\": \"wildmap\",\"main\": \"electronmain.js\",\"launch\": "\"http://$serverip:$serverport/?id=MASTER&sizex=$masterwidth&sizey=$masterheight&wallwidth=$wallwidth&wallheight=$wallheight&syncserver=$serverip&viewSyncPort=$viewSyncPort&shipUpdatePort=$shipUpdatePort&tilesource=http://$serverip:$tileport&tuioserver=$serverip&tuioport=$tuioport&ESRI=$esri&infoBoxScale=$infoBoxScaleMaster&shipSymbolSize=$shipSymbolSizeMaster\"",\"window\": {\"kiosk\": false,\"fullscreen\":false,\"width\": $masterwidth,\"height\": $masterheight},\"additional_trust_anchors\": \"$jitsiCertificate\"} > package.json

echo "Open the following lin on chromium"
echo  "http://$serverip:$serverport/?id=MASTER&sizex=$masterwidth&sizey=$masterheight&wallwidth=$wallwidth&wallheight=$wallheight&syncserver=$serverip&viewSyncPort=$viewSyncPort&shipUpdatePort=$shipUpdatePort&tilesource=http://$serverip:$tileport&tuioserver=$serverip&tuioport=$tuioport&ESRI=$esri&infoBoxScale=$infoBoxScaleMaster&shipSymbolSize=$shipSymbolSizeMaster"


#electron .
