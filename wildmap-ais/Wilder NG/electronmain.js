// Modules to control application life and create native browser window
const { app, BrowserWindow, session } = require('electron')
const path = require('node:path')
const fs = require('node:fs');

function createWindow() {

  
  //session.defaultSession.clearStorageData(null, (error) => {
    fs.readFile('package.json', 'utf8', (err, data) => {
      if (err) {
        console.error(err);
        return;
      }
  
      let json = JSON.parse(data);
      console.log(">>> Launching " + json.launch);
      console.log(">>> Size " + json.window.width + "x" + json.window.height);
  
      // Create the browser window.
      const mainWindow = new BrowserWindow({
        width: json.window.width,
        height: json.window.height,
        webPreferences: {
          preload: path.join(__dirname, 'preload.js'),
          allowRunningInsecureContent: true
        }
      })
  
      //if (json.window.kiosk)
      mainWindow.setKiosk(false);
      mainWindow.setPosition(0,0);
      // and load the index.html of the app.
      mainWindow.loadURL(json.launch);
  
      // Open the DevTools.
       mainWindow.webContents.openDevTools()
    });
//  });
}

// This method will be called when Electron has finished
// initialization and is ready to create browser windows.
// Some APIs can only be used after this event occurs.
app.whenReady().then(() => {
  createWindow()

  app.on('activate', function () {
    // On macOS it's common to re-create a window in the app when the
    // dock icon is clicked and there are no other windows open.
    if (BrowserWindow.getAllWindows().length === 0) createWindow()
  })
})

// Quit when all windows are closed, except on macOS. There, it's common
// for applications and their menu bar to stay active until the user quits
// explicitly with Cmd + Q.
app.on('window-all-closed', function () {
  if (process.platform !== 'darwin') app.quit()
})

// In this file you can include the rest of your app's specific main process
// code. You can also put them in separate files and require them here.
