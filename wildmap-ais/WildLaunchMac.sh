
offsetX=0
offsetY=0
wcol=4
wrow=4
wtile_w=15360
wtile_h=4320
viewSyncPort=8080
serverport=8081
tileport=8082
tuioport=8083
shipUpdatePort=8084
nwpath="nw"
user=wild
wallwidth=61440
wallheight=17280
#proxyaddress="192.168.2.254:8086"

#master canvas size. MUST be the same ratio than the wall
masterwidth=1536
masterheight=432


packagefile="package.json"
nodetmppath="/home/wild/tmp/wildmap"
nodeuserdirpath="/home/wild/.cache/wildmap"

servertmppath="/Users/gladin/tmp/"
serverpackagepath=$servertmppath$packagefile
serverip="192.168.2.125"
clientcachedir="/media/ssd/cache/wildmap"
tileserverip="127.0.0.1"

# 16-level pyramid using ESRI below 8
esri=true
esriaddress=http://192.168.2.254:8086/arcgis #Use reverse proxy



shipSymbolSize=2
shipSymbolSizeMaster=2
shipStrokeSize=2
shipPathStrokeSize=3
infoBoxScale=1
infoBoxScaleMaster="0.1"

jitsiCertificate="-----BEGIN CERTIFICATE-----
                 MIIDKDCCAhCgAwIBAgIURtRwnoavj2iBbqYAv13Y+E2TKmowDQYJKoZIhvcNAQEL
                 BQAwFzEVMBMGA1UEAwwMMTkyLjE2OC4yLjM1MB4XDTIzMDIwOTE1MzEyOVoXDTI0
                 MDIwOTE1MzEyOVowFzEVMBMGA1UEAwwMMTkyLjE2OC4yLjM1MIIBIjANBgkqhkiG
                 9w0BAQEFAAOCAQ8AMIIBCgKCAQEAyFUt3mE5g0xHDjSZbxl2UqON0Nsdc4jMLHcT
                 l1TF5C54PuvQzsQVlnRGJMiV0tQOWmgy+LuZTEu48k3Uaf8tPc6vlgzr2cMU8nRs
                 UZPsv0jn+yO1RCRMZZWPYUEn62tR1tKVrgmSpGhDgtkPfVBz5V7tQSEemTPUXqJ+
                 OlCN/k+Uq7idL42sZoN6v4zKf/qAHn9m/yIYV53KIeqfudAOdv5KGKIeX6SwrwF4
                 mLnQWf5kRnqgU4u+GX4bgqoNNnt44147+ZKJ8tc+SUwH8y4u9kiijt1bHDepX019
                 TL1CfVgw+InjrPw1NuRDkwT/fYOQHY0jhIIVU+tCNiQUhkvh7QIDAQABo2wwajAL
                 BgNVHQ8EBAMCBeAwEwYDVR0lBAwwCgYIKwYBBQUHAwEwJwYDVR0RBCAwHoIJaG9t
                 ZS5hcnBhggsqLmhvbWUuYXJwYYcEwKgCIzAdBgNVHQ4EFgQUfAtBde3iuKaFH2uP
                 ksauBzUsnE4wDQYJKoZIhvcNAQELBQADggEBAJyAxiRLBes1Q+/mX9kmmOesGWxL
                 xDJQ1f67Xlo1A/kk7Y7Uq02m5V9dHOkb1nlO3witlkbVDziWvKIxQB9XEci5a/VW
                 u0ssrIrxoDDlrfljUiBcl4P5goJ955WkqB8ufQcfyeIaQ9tvqcBsSHTjg6uE0rGJ
                 m2ashlZ94nQvg2d1S5jp8SyvipMgRK3fMkTp/LpkLl0EdwjHIuMGwZP7NtsfTuX3
                 91nKpSYf46LP2/rNyDd8UCKJfWKsr+J3URQ7zWvkBqHnEiFHFls0SfR6eJiweB8I
                 f4dqmzGUCFAhk5AsYuY1Kvc6d+GZMEePV3iGbKORax9PhM/rFMr5l/wRpwI=
                 -----END CERTIFICATE-----"

launchCluster=""

if [ "$launchCluster" ];

then

echo "clear nw cache"

walldo rm -r $nodeuserdirpath
walldo rm -r $nodetmppath
walldo rm -r $clientcachedir

sleep 1
walldo mkdir $nodetmppath
walldo mkdir $clientcachedir

sleep 1

echo "start nw on nodes"
for col in {a..d}
do
  for row in {1..4}
    do
      echo  {\"name\": \"wildmap\",\"main\": "\"http://$serverip:$serverport/?id=$col$row&offsetx=$offsetX&offsety=$offsetY&wallx=$wcol&wally=$wrow&wallwidth=$wallwidth&wallheight=$wallheight&sizex=$wtile_w&sizey=$wtile_h&tilesource=http://$serverip:$tileport&syncserver=$serverip&viewSyncPort=$viewSyncPort&shipUpdatePort=$shipUpdatePort&ESRI=$esri&esriaddress=$esriaddress&shipSymbolSize=$shipSymbolSize&shipStrokeSize=$shipStrokeSize&shipPathStrokeSize=$shipPathStrokeSize&infoBoxScale=$infoBoxScale\"",\"window\": {\"kiosk\": true,\"fullscreen\": true,\"width\": $wtile_w,\"height\": $wtile_h},\"additional_trust_anchors\": \"$jitsiCertificate\"} > $serverpackagepath$col$row
      scp $servertmppath$col$row $user@$col$row.wild.lri.fr:$nodetmppath/$packagefile
      ssh $user@$col$row.wild.lri.fr DISPLAY=:0 $nwpath $nodetmppath --disk-cache-dir=$clientcachedir&
      sleep 0.2
      offsetY=$((offsetY+wtile_h))
    done
    offsetX=$((offsetX+wtile_w))
    offsetY=0
done
fi

echo  {\"name\": \"wildmap\",\"main\": "\"http://$serverip:$serverport/?id=MASTER&sizex=$masterwidth&sizey=$masterheight&wallwidth=$wallwidth&wallheight=$wallheight&tilesource=http://$serverip:$tileport&syncserver=$serverip&viewSyncPort=$viewSyncPort&tuioserver=$serverip&tuioport=$tuioport&shipUpdatePort=$shipUpdatePort&ESRI=$esri&esriaddress=$esriaddress&shipSymbolSize=$shipSymbolSizeMaster\"",\"window\": {\"kiosk\": false,\"fullscreen\":false,\"width\": $masterwidth,\"height\": $masterheight},\"additional_trust_anchors\": \"$jitsiCertificate\" } > $serverpackagepath

./client/node_modules/nw/bin/nw $servertmppath
