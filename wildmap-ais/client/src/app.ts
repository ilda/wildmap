import OpenSeadragon, {Placement, Point} from "openseadragon";
import io from "socket.io-client";
import * as d3 from "d3";
import Color from "color";
import {WildOsdOptions, WildOsd, Rpc, WebGlOverlay, Canvas2dOverlay, PieMenu, SvgOverlay, FixedOverlay} from "wildosd";
import randomColor from "randomcolor";

import JitsiMeetJS from "../lib/lib-jitsi-meet.min.js";

const SHIP_TYPE = {
	'1': "Reserved",
	'2': "Wing In Ground",
	'3': "Special/3",
	'4': "High-Speed",
	'5': "Special/5",
	'6': "Passenger",
	'7': "Cargo",
	'8': "Tanker",
	'9': "Other"
};

JitsiMeetJS.setLogLevel(JitsiMeetJS.logLevels.ERROR);
const jitsiServerURL = 'jitsi-exsitu.lisn.upsaclay.fr';
const jitsiConnectionOptions = {
	hosts: {
		domain: jitsiServerURL,
		muc: `conference.${jitsiServerURL}`,
		anonymousdomain:'guest.'+jitsiServerURL
	},
	serviceUrl: `https://${jitsiServerURL}/http-bind`,
	clientNode: `https://${jitsiServerURL}`,
};
const jitsiConferenceName = "continuum";
JitsiMeetJS.init();



const queryString = window.location.search;
const urlParams = new URLSearchParams(queryString);

console.log(`url parameters ${urlParams}`);

let useESRI: boolean = false;
let id: string = "";
const useWebGl = true;


// tile pyramid max level (z)
let TP_MAX_Z = 8;
// tile size in px
const TP_TILE_SIZE = 256;
// total width & height of tile pyramid
let TP_WIDTH = TP_TILE_SIZE * Math.pow(2, TP_MAX_Z);
let TP_HEIGHT = TP_TILE_SIZE * Math.pow(2, TP_MAX_Z);

let SHIP_SYMBOL_SIZE: number  = 2;
let SHIP_STROKE_SIZE: number = 0.5;
let SHIP_PATH_STROKE_SIZE: number = 2;

const HALF_PI = Math.PI / 2;
const shipAngleDelta = Math.PI / 6; // half the angle for ship triangles

let shipMap = new Map<string, LiveShipInfo>(); //Ship
let shipTrailMap = new Map<string, Array<Array<number>>>();

let updateLines = false;

enum SelectionType {
	InfoBox = 1,
	PositionHistory = 2,
}

let selectionType: SelectionType = SelectionType.InfoBox;

let pieMenuRadius: number;
let pieMenuSize: number = 5; //Percent of the height of the wall

let syncServerIp = "127.0.0.1";
if (urlParams.has('syncserver')) {
	syncServerIp = <string>urlParams.get('syncserver');
}

let viewSyncPort = 8080;
if (urlParams.has('viewSyncPort')) {
	viewSyncPort = +<string>urlParams.get('viewSyncPort');
}

let shipUpdatePort = 8084;
if (urlParams.has('shipUpdatePort')) {
	shipUpdatePort = +<string>urlParams.get('shipUpdatePort');
}

let tuioServerIp = "127.0.0.1";
if (urlParams.has('tuioserver')) {
	tuioServerIp = <string>urlParams.get('tuioserver');
}

let tuioPort = 8083;
if (urlParams.has('tuioport')) {
	tuioPort = +<string>urlParams.get('tuioport');
}


let offsetX: number = 1;
if (urlParams.has('offsetx')) {
	// @ts-ignore
	offsetX = +urlParams.get('offsetx');
}
let offsetY: number = 1;
if (urlParams.has('offsety')) {
	// @ts-ignore
	offsetY = +urlParams.get('offsety');
}


let sizeX: number = 800.0;
let sizeY: number = 600.0;

if (urlParams.has('sizex')) {
	// @ts-ignore
	sizeX = +urlParams.get('sizex');
}
if (urlParams.has('sizey')) {
	// @ts-ignore
	sizeY = +urlParams.get('sizey');
}

let wallWidth: number = 800.0;
let wallHeight: number = 600.0;

if (urlParams.has('wallwidth')) {
	// @ts-ignore
	wallWidth = +urlParams.get('wallwidth');
}
if (urlParams.has('wallheight')) {
	// @ts-ignore
	wallHeight = +urlParams.get('wallheight');
}

//Set nw position
if (urlParams.has('posx')) {
	// @ts-ignore
	let posx = +urlParams.get('posx');
	console.log("posx "+posx);
	WildOsd.setWindowPosition(posx,0);
}


let dataSourceUrl: string = "http://127.0.0.1:8082";
let tileSourceUrl: string = `${dataSourceUrl}`;
let shipImgSourceUrl: string = `${dataSourceUrl}/staticShipData`;
if (urlParams.has('tilesource')) {
	// @ts-ignore
	dataSourceUrl = urlParams.get('tilesource');
	tileSourceUrl = `${dataSourceUrl}/shipmap_world_sm`;
	shipImgSourceUrl = `${dataSourceUrl}/staticShipData`;
}

let tileSourceExt: string = "png";
if (urlParams.has('tileext')) {
	// @ts-ignore
	tileSourceExt = urlParams.get('tileext');
}

let tileFunc = function (z:number, x:number, y:number):string {
	 return `${tileSourceUrl}/${z}/${x}/${(Math.pow(2,z)-y-1)}.${tileSourceExt}`;
};

if (urlParams.has('shipSymbolSize')) {
	let sssStr: string | null = urlParams.get('shipSymbolSize');
	if (sssStr) {
		let sss: number = parseFloat(sssStr);
		if (sss && sss > 0) {
			SHIP_SYMBOL_SIZE = sss;
			console.log(`Setting ship symbol size to ${SHIP_SYMBOL_SIZE}`);
		} else {
			console.log(`Value for ship symbol size should be > 0. Received ${sssStr}.`);

		}
	}
}

if (urlParams.has('shipStrokeSize')) {
	let sssStr: string | null = urlParams.get('shipStrokeSize');
	if (sssStr) {
		let sss: number = parseFloat(sssStr);
		if (sss >= 0) {
			SHIP_STROKE_SIZE = sss;
			console.log(`Setting ship stroke size to ${SHIP_STROKE_SIZE}`);
		} else {
			console.log(`Value for ship stroke size should be >= 0. Received ${sssStr}.`);
		}
	}
}

if (urlParams.has('shipPathStrokeSize')) {
	let spssStr: string | null = urlParams.get('shipPathStrokeSize');
	if (spssStr) {
		let spss: number = parseFloat(spssStr);
		if (spss >= 0) {
			SHIP_PATH_STROKE_SIZE = spss;
			console.log(`Setting ship path stroke size to ${SHIP_PATH_STROKE_SIZE}`);
		} else {
			console.log(`Value for ship path stroke size should be >= 0. Received ${spssStr}.`);
		}
	}
}

const InfoBox = {
	WIDTH: 256,
	SHIP_THUMB_WIDTH: 256 / 1.1,
	HEIGHT: 240,
	MARGIN_LEFT: 14,
	MARGIN_RIGHT: 24,
	SCALE_FACTOR: 0.5,
	DEFAULT_X_OFFSET: 10,
	DEFAULT_Y_OFFSET: 10,
	X_OFFSET: 10,
	Y_OFFSET: 10
};

if (urlParams.has('infoBoxScale')) {
	let ibsStr: string | null = urlParams.get('infoBoxScale');
	if (ibsStr) {
		let ibs: number = parseFloat(ibsStr);
		if (ibs && ibs > 0) {
			InfoBox.SCALE_FACTOR = ibs;
			InfoBox.X_OFFSET = InfoBox.DEFAULT_X_OFFSET * InfoBox.SCALE_FACTOR;
			InfoBox.Y_OFFSET = InfoBox.DEFAULT_Y_OFFSET * InfoBox.SCALE_FACTOR;
			console.log(`Setting infobox size scale to ${InfoBox.SCALE_FACTOR}`);
		} else {
			console.log(`Value for infobox scale factor should be > 0. Received ${ibsStr}.`);

		}
	}
}

let esriAddress = "http://services.arcgisonline.com/arcgis";
if (urlParams.has('esriaddress')) {
	esriAddress = <string>urlParams.get('esriaddress');
}

if (urlParams.has('ESRI')) {
	let esriB = urlParams.get('ESRI');
	if (esriB) {
		useESRI = (esriB.toLowerCase() === "true");
	}
	if (useESRI) {
		TP_MAX_Z = 16;
		TP_WIDTH = TP_TILE_SIZE * Math.pow(2, TP_MAX_Z);
		TP_HEIGHT = TP_TILE_SIZE * Math.pow(2, TP_MAX_Z);
		tileFunc = function (z, x, y) {
			if (z <= 8) {
				// shipmap from 0 to 8
				return `${tileSourceUrl}/${z}/${x}/${Math.pow(2, z) - y - 1}.png`;
			} else {
				// ESRI ocean from 8 to 16
				return esriAddress + `/rest/services/Ocean/World_Ocean_Base/MapServer/tile/${z}/${y}/${x}`;
			}
		}
	}
}


let el = document.getElementById("seadragon-viewer");
if (el) {
	el.style.width = `${sizeX}px`;
	el.style.height = `${sizeY}px`;

	console.log(`Canvas size is ${sizeX}px x ${sizeY}px`);
} else {
	el = document.body;
	console.error('Cannot find elem seadragon-viewer');
}


// const socketViewSync: SocketIOClient.Socket = io(`http://${syncServerIp}:${viewSyncPort}`);
const socketShipUpdate: SocketIOClient.Socket = io(`http://${syncServerIp}:${shipUpdatePort}`);


if (urlParams.has('id')) {
	// @ts-ignore
	id = urlParams.get('id');
} else {
	socketShipUpdate.emit("message", "ID,ERROR");
}


let wildOsdOptions: WildOsdOptions =
	{
		id: id,
		useWebGl: true,
		osdPyramidLevel: TP_MAX_Z,
		osdTileSize: TP_TILE_SIZE,
		syncServerIp: syncServerIp,
		syncServerPort: viewSyncPort,
		wallWidth: wallWidth,
		wallHeight: wallHeight,
		tileWidth: sizeX,
		tileHeight: sizeY,
		tileOffsetX: offsetX,
		tileOffsetY: offsetY,
		osdTileUrl: tileFunc,
		selectionEnd: selectionEnd,
		tuioServerIp: tuioServerIp,
		tuioServerPort: tuioPort,
		clickDistThreshold: 50,
		dblClickDistThreshold: 50
	};

let wildOsd = new WildOsd("Main", el, wildOsdOptions);


/* ship fill color by type*/
const shipTypeColors = [ '#eee', '#f0027f',
	'#f0027f', '#ffff99', '#f0027f', '#7fc97f', '#beaed4', '#fdc086', '#bf5b17','#000000'];

/* get ship color as rgb arrays */
let shipTypeColorsRGB = new Array<Array<number>>();
shipTypeColorsRGB.push(new Color("red").unitArray());//ship index begins at 1
for (let color of shipTypeColors) {
	shipTypeColorsRGB.push(new Color(color).unitArray());
}


function drawShip(d: LiveShipInfo, ctx: CanvasRenderingContext2D, zoom: number, clickedPosition: Point | null): boolean {
	const xy = WM_PROJ([d.lon, d.lat]);
	const x = xy[0] * TP_WIDTH;
	const y = xy[1] * TP_HEIGHT;

	let angle = 0;
	if (d.bearing != null && d.bearing != 0) {
		angle = d.bearing * Math.PI / 180 + HALF_PI;
	}

	const size = SHIP_SYMBOL_SIZE / zoom;

	const p1 = new Point(x + size * 2 * Math.cos(angle), y + size * 2 * Math.sin(angle));
	const p2 = new Point(x + size * Math.cos(angle + shipAngleDelta), y + size * Math.sin(angle + shipAngleDelta));
	const p3 = new Point(x + size * Math.cos(angle + Math.PI - shipAngleDelta), y + size * Math.sin(angle + Math.PI - shipAngleDelta));
	const p4 = new Point(x + size * Math.cos(angle + Math.PI + shipAngleDelta), y + size * Math.sin(angle + Math.PI + shipAngleDelta));
	const p5 = new Point(x + size * Math.cos(angle - shipAngleDelta), y + size * Math.sin(angle - shipAngleDelta));

	ctx.beginPath();
	ctx.strokeStyle = "black";
	ctx.fillStyle = shipTypeColors[d.stype];
	ctx.lineWidth = SHIP_STROKE_SIZE;
	ctx.moveTo(p1.x, p1.y);
	ctx.lineTo(p2.x, p2.y);
	ctx.lineTo(p3.x, p3.y);
	ctx.lineTo(p4.x, p4.y);
	ctx.lineTo(p5.x, p5.y);
	ctx.lineTo(p1.x, p1.y);
	ctx.stroke();
	ctx.fill();

	if (clickedPosition == null)
		return false;
	else {
		//"coordinate of the point to check, unaffected by the current transformation of the context"
		return ctx.isPointInPath(clickedPosition.x, clickedPosition.y);
	}
}

function drawShipTrail(positions: Array<Array<number>>, id: string, ctx: CanvasRenderingContext2D) {
	ctx.beginPath();
	ctx.strokeStyle = "black";
	ctx.lineWidth = SHIP_STROKE_SIZE * 1000;

	for (let i = 0; i < positions.length; i++) {
		if (i == 0)
			ctx.moveTo(positions[i][0], positions[i][1]);
		else
			ctx.lineTo(positions[i][0], positions[i][1]);
	}
	ctx.stroke();
}


//PIE MENU
if (wildOsd.isMaster) {
	pieMenuRadius = pieMenuSize * wallHeight / 100 * sizeX / wallWidth;
} else {
	pieMenuRadius = pieMenuSize * wallHeight / 100;
}
let pieMenu = new PieMenu(pieMenuRadius);


let overlayRootDiv = wildOsd.osdViewer.canvas.children[1]; //Created by osd

let menuOverlay = new FixedOverlay(wildOsd,wildOsd.overlayRootDiv,pieMenu.root);

pieMenu.addSlice('#222', 33, "Ship Details", () => {
	if (!wildOsd.isSelectionMode)
		wildOsd.startSelectionMode();
	selectionType = SelectionType.InfoBox;
	pieMenuHide();
});
// pieMenu.addSlice('#222', 25, "Map Nav.", () => {
// 	if (wildOsd.isSelectionMode)
// 		wildOsd.endSelectionMode();
//
// 	pieMenuHide();
// 	wildOsd.overlayRemove(wildOsd.SELECTION_ID);
// 	wildOsd.rpCall("overlayRemove", [wildOsd.SELECTION_ID]);
//
// });
pieMenu.addSlice('#222', 33, "Ship Pos.", () => {
	if (!wildOsd.isSelectionMode)
		wildOsd.startSelectionMode();
	selectionType = SelectionType.PositionHistory;
	pieMenuHide();
});

pieMenu.addSlice('#222', 34, "Window", () => {
	pieMenuSetLayer("dragmag");
});

let dragMagId = 0;

pieMenu.addSlice('#222', 25, "Remove", () => {
	let x = parseInt(pieMenu.root!.style.left) + pieMenu.r;
	let y = parseInt(pieMenu.root!.style.top) + pieMenu.r;
	let windowName = wildOsd.removeWindowByPosition(x, y);
	d3Names = d3Names.filter((name)=> { return name !== windowName })
	pieMenuSetLayer(PieMenu.pieMenuDefaultLayer);
	pieMenuHide();
}, "dragmag");

pieMenu.addSlice('#222', 25, "Add Window", () => {
	let x = parseInt(pieMenu.root!.style.left) + pieMenu.r;
	let y = parseInt(pieMenu.root!.style.top) + pieMenu.r;

	initD3ShipPerType(x,y,150,150);
	if (wildOsd.isMaster) {
		let rect = wildOsd.getWallPosition(x,y,150,150);
		wildOsd.rpCall("initD3ShipPerType", [rect.x,rect.y,rect.w,rect.h]);
	}

	pieMenuSetLayer(PieMenu.pieMenuDefaultLayer);
	pieMenuHide();
}, "dragmag");

pieMenu.addSlice('#222', 25, "JITSI", () => {
	let x = parseInt(pieMenu.root!.style.left) + pieMenu.r;
	let y = parseInt(pieMenu.root!.style.top) + pieMenu.r;

	jitsiInit();
	if (wildOsd.isMaster) {
		wildOsd.rpCall("jitsiInit",[]);
	}

	pieMenuSetLayer(PieMenu.pieMenuDefaultLayer);
	pieMenuHide();
}, "dragmag");

pieMenu.addSlice('#222', 25, "Add Dragmag", () => {
	let x = parseInt(pieMenu.root!.style.left) + pieMenu.r;
	let y = parseInt(pieMenu.root!.style.top) + pieMenu.r;
	let w = 200;
	let h = 200

	let color = randomColor();

	// Add new dragmag before menu div
	let dragMagViewer = wildOsd.addDragMagOsd(pieMenu.root.id, "dragmag" + dragMagId, color, wildOsdOptions, x, y, w, h);

	//Display ships only on this dragmap
	wildOsd.dragMagAddOverlay('dragmag' + dragMagId, shipOverlay);

	if (wildOsd.isMaster) {
		let rect = wildOsd.getWallPosition(x,y,w,h);
		wildOsd.rpCall('createDragMagClient', [pieMenu.root.id, "dragmag" + dragMagId, color, rect.x, rect.y, rect.w, rect.h]);
	}
	pieMenuSetLayer(PieMenu.pieMenuDefaultLayer);
	pieMenuHide();
	dragMagId++;

}, "dragmag");


function createDragMagClient(rootId: string, id: string, color: string, x: number, y: number, width: number, height: number) {

	// Add new dragmag before menu div
	let dragMagViewer = wildOsd.addDragMagOsd(rootId, id, color, wildOsdOptions, x - wildOsdOptions.tileOffsetX, y - wildOsdOptions.tileOffsetY, width, height);

	//Display ships only on this dragmap
	wildOsd.dragMagAddOverlay(id, shipOverlay);

}

wildOsd.rpRegister("createDragMagClient", createDragMagClient);

pieMenu.init();
pieMenu.hide();


//SVG OVERLAY (Ship info panels)
let panelsOverlay = new SvgOverlay(wildOsd.osdViewer, overlayRootDiv, "ShipOverlay");
d3.select(panelsOverlay.node)
	.append("g")
	.attr("id", "shipInfo");


let shipOverlay;
if (useWebGl) {
	// WEBGLOVERLAY(ships and paths)
	shipOverlay = new WebGlOverlay(wildOsd.osdViewer, {}, overlayRootDiv, shipTypeColorsRGB);
} else {
	// CANVAS OVERLAY(ships and paths)
	shipOverlay = new Canvas2dOverlay(wildOsd.osdViewer, {}, overlayRootDiv,
		(index: number, context: CanvasRenderingContext2D | null, x: number, y: number, zoom: number) => {
			if (!context) return;
			let hit = false;
			let lastHitShipInfo: LiveShipInfo | null = null;
			shipMap.forEach((d) => {
				if (checkShipClick) {
					hit = drawShip(d, context, zoom, lastClickPos);
					if (hit) lastHitShipInfo = d;
				} else {
					drawShip(d, context, zoom, null);
				}
			});
			if (lastHitShipInfo != null && wildOsd.isMaster) {
				requestShipsDetailDisplay(lastHitShipInfo);
			}
			checkShipClick = false;
			shipTrailMap.forEach((positions, id: string) => drawShipTrail(positions, id, context));
		});
}


const WM_PROJ = d3.geoMercator()
	.center([0, 0])
	.translate([0.5, 0.5])
	.scale(1 / (2 * Math.PI));


interface LiveShipInfo {
	id: string;
	lat: number;
	lon: number;
	bearing: number;
	speed: number;
	dwt: number;
	length: number;
	flag: string;
	dest: string;
	stype: string;
	name: string;
	mmsi: string;
	imgUrl: string;
}

interface StaticShipInfo {
	id: string;
	name: string;
	mmsi: string;
	callsign: string;
	imgUrl: string;
}

// when InfoBox is manually repositioned on map,
// store that offset so that it can be enforced
// when zooming
interface InfoBoxOffset {
	ibxo: number;
	ibyo: number;
}

type AllShipInfo = StaticShipInfo & LiveShipInfo & InfoBoxOffset;

socketShipUpdate.on("Rpc", (funcToCall: Rpc) => {
	let code = funcToCall.func;
	window[code].apply(this, funcToCall.args);
});

function generateShipSymbol(size:number, angle:number){
	const p1 = new Point(size * 2 * Math.cos(angle), size * 2 * Math.sin(angle));
	const p2 = new Point(size * Math.cos(angle + shipAngleDelta), size * Math.sin(angle + shipAngleDelta));
	const p3 = new Point(size * Math.cos(angle + Math.PI - shipAngleDelta), size * Math.sin(angle + Math.PI - shipAngleDelta));
	const p4 = new Point(size * Math.cos(angle + Math.PI + shipAngleDelta), size * Math.sin(angle + Math.PI + shipAngleDelta));
	const p5 = new Point(size * Math.cos(angle - shipAngleDelta), size * Math.sin(angle - shipAngleDelta));
	return [[p1.x, p1.y], [p2.x, p2.y], [p3.x, p3.y], [p4.x, p4.y], [p5.x, p5.y]];
}

let shipDataJson: Array<LiveShipInfo> = [] as LiveShipInfo[];
let shipNumberPerType: Array<number> = new Array<number>(Object.keys(SHIP_TYPE).length).fill(0);

//give an id to the div generated by osd that is the root for overlays
d3.select(overlayRootDiv)
	.attr("id","overlayRootDiv")
	.style("width","100%")
	.style("height","100%");


let d3Names: Array<string> = [] as string[];
let windowsNamesSuffix = 0;

function initD3ShipPerType(posX:number, posY:number, width:number, height:number) {
	const d3Name = "d3NumberperType"+(windowsNamesSuffix++);
	d3Names.push(d3Name);

	console.log("add " + d3Name)

	wildOsd.addWindow("PieMenu",d3Name,"red",posX,posY,width,height);

	let d3WindowDiv = d3.select("#"+d3Name);
	let d3WindowTitle = d3Name+"Title"

	let svg = d3WindowDiv
		.append("svg")
		.attr("id","d3GraphRoot")
		.attr("width", "100%")
		.attr("height", "100%")
		.style("background","lightgrey");

	let title = svg.append("g")
		.append("text")
		.attr("id",d3WindowTitle)
		.attr("text-anchor","middle")
		.text("Ships per Type")
		.style("font-family","monospace")

	let graphRoot = svg.append("g");

	wildOsd.getWindow(d3Name)!.onSetTransform= (x: number, y: number, w: number, h: number) => {
		shipNumberPerType.fill(0);
		shipMap.forEach(d => {
			let stype = parseInt(d.stype) - 1; //start at 1
			if (Number.isInteger(stype) && stype >0 && stype < shipNumberPerType.length)
				shipNumberPerType[stype]++;
		});

		let transitionTime = 0; //dragmag resize
		if (x==0 && y==0)
			transitionTime = 1000; //data update
		if (h == 0)
			h = d3WindowDiv.style("height").replace("px", "") as number;
		if (w == 0)
			w = d3WindowDiv.style( "width").replace("px", "") as number;

		let yScale = d3.scaleLinear().domain([0,shipMap.size])
			.range([0, h-1]);

		let barWidth = w/shipNumberPerType.length;

		title.style("font-size",barWidth+"px")
			.attr("x", w/2)
			.attr("y", h/10)

		graphRoot.selectAll("rect")
			.data(shipNumberPerType)
			.join(
				enter => {
					return enter
						.append("rect")
						.attr("width", barWidth)
						.attr("height", d => yScale(d) )
						.attr("x", (d,i) => i*barWidth )
						.attr("y", d => h-yScale(d))
						.attr("fill",(d,i) => shipTypeColors[i])
				},
				update => {
					return update
						.transition()
						.duration(transitionTime)
						.attr("width", barWidth)
						.attr("height", d => yScale(d) )
						.attr("x", (d,i) => i*barWidth )
						.attr("y", d => h-yScale(d))
				},
				exit => {
					return exit
					//.remove("rect")
				}

			)

		graphRoot.selectAll("text")
			.data(shipNumberPerType)
			.join(
				enter => {
					return enter
						.append("text")
						.style("font-size",barWidth/2+"px")
						.style("font-family","monospace")
						.attr("x", (d,i) => i*barWidth+barWidth/2 )
						.attr("y", d => 3*h/4)
						.attr('transform',(d,i) => `rotate(270 ${barWidth*i+barWidth/2}  ${3*h/4})`)
						.text((d,i) => `${SHIP_TYPE[i+1]}:${d}`)
				},
				update => {
					return update
						.transition()
						.style("font-size",barWidth/2+"px")
						.duration(transitionTime)
						.attr("x", (d,i) => i*barWidth+barWidth/2 )
						.attr("y", d => 3*h/4)
						.attr('transform',(d,i) => `rotate(270 ${barWidth*i+barWidth/2}  ${3*h/4})`)
						.text((d,i) => `${SHIP_TYPE[i+1]}:${d}`)
				}
			)
	};

	wildOsd.windowSetTransform(d3Name,posX,posY,width,height);

}

wildOsd.rpRegister("initD3ShipPerType",initD3ShipPerType);

let jistiUsersBlackList = ["a1","a2","a3","a4","a5","b1","b2","b3","b4","b5"];

function jitsiInit () {
	let conference;
	let participants;

	console.log("jitsiInit");

	let connection = new JitsiMeetJS.JitsiConnection(null, null, jitsiConnectionOptions);
	connection.addEventListener(JitsiMeetJS.events.connection.CONNECTION_ESTABLISHED, onConnectionSuccess);
	connection.addEventListener(JitsiMeetJS.events.connection.CONNECTION_FAILED, onConnectionFailed);
	connection.addEventListener(JitsiMeetJS.events.connection.CONNECTION_DISCONNECTED, disconnect);
	connection.connect();

	function onConnectionSuccess(data) {
		console.log("onConnectionSuccess");
		conference = connection.initJitsiConference(jitsiConferenceName, {
			useIPv6: false,
			preferredCodec: "VP9",
			enableAnalyticsLogging: false
		});
		conference.on(JitsiMeetJS.events.conference.TRACK_ADDED, onRemoteTrack);
		conference.on(JitsiMeetJS.events.conference.TRACK_REMOVED, onRemoteTrackRemoved);
		conference.on(JitsiMeetJS.events.conference.REMOTE_STATS_UPDATED, onRemoteTrackUpdated);
		conference.on(JitsiMeetJS.events.conference.CONFERENCE_JOINED, onConferenceJoined);
		conference.on(JitsiMeetJS.events.conference.USER_LEFT, onUserLeft);
		conference.on(JitsiMeetJS.events.conference.USER_JOINED, onUserJoined);
		conference.on(JitsiMeetJS.events.conference.ENDPOINT_MESSAGE_RECEIVED, onMessage);


		conference.setDisplayName(id);
		conference.join();
	}

	function onMessage(header,data) {
			console.log(data);

	}

		function onConnectionFailed(data) {
		console.log("onConnectionFailed");
		console.log(data);
	}

	function disconnect(data) {
		console.log("disconnect");
		console.log(data);
	}

	function onConferenceJoined(data) {
		console.log("onConferenceJoined " + id + ">");
		conference.sendTextMessage(id + " JOINED");


		setInterval(()=> {

			//Test
			conference.broadcastEndpointMessage(
				{ cmd: 2, data: [ 1.100000023841858, 1, 2, 3, 4, 5 ] }
			);


		},2000);
		conference.setReceiverConstraints({
			"lastN": -1, // maximum number of video streams => no limit
			'defaultConstraints': { 'maxHeight': -1 } // Default resolution requested for all endpoints => no constraints
		})
	}

	function onRemoteTrackRemoved(track) {
		console.log("onRemoteTrackRemoved");

	}

	function onUserJoined(id) {

		participants = conference.getParticipants();

	}
	function onUserLeft(id) {
		console.log('onUserLeft');
		for (const participant of participants) {
			if (participant.getId() == id) {
				let participantId = getValidParticipantId(participant);
				console.log("Removing windows for remote jitsi user:" + participantId);
				wildOsd.removeWindowById(participantId);
				break;
			}
		}
	}

	function onRemoteTrackUpdated(id, stats) {
		console.log("onRemoteTrackUpdated");
		console.log(stats);
	}

	function getValidParticipantId(participant) {
		if (participant.getDisplayName())
			return participant.getDisplayName();
		else
			return "_" + participant.getId(); //querySelector method uses CSS3 selectors for querying the DOM and CSS3 doesn't support ID selectors that start with a digit
	}


	function onRemoteTrack(track) {
		console.log("onRemoteTrack");
		if (track.isLocal()) {
			return;
		}

		let participants = conference.getParticipants();

		//Is this a right stream ?
		let rightRemoteUser = false;
		let participantDisplayname;
		for (const participant of participants) {
			if (participant.getId() == track.getParticipantId()) {
				if (jistiUsersBlackList.indexOf(participant.getDisplayName()) == -1) {
					rightRemoteUser = true;
					participantDisplayname = getValidParticipantId(participant);
					break;
				}
			}
		}

		if (track.getType() == "video" && rightRemoteUser && participantDisplayname.length!=0) {
			//Create the window
			console.log("Creating windows for remote jitsi user:" + participantDisplayname);
			if (!document.getElementById( participantDisplayname)) {
				//Create the window
				let wX=0;
				let wY=0;
				let wWidth = 200;
				let wHeight = 200;
				wildOsd.addWindow("PieMenu", participantDisplayname, "red", wX, wY, wWidth, wHeight,
					() => {
						console.log("LEAVE");
					});
				let videoWindowDiv = d3.select("#" + participantDisplayname);
				let video = videoWindowDiv.append("video")
					.attr("id", participantDisplayname + "video")
					.style("width", "100%")
					.style("height", "100%");

				wildOsd.windowSetTransform(participantDisplayname,wX, wY, wWidth, wHeight);

			}

			let elem = document.getElementById(participantDisplayname + "video") as HTMLVideoElement;
			if (!wildOsd.isMaster)
				elem.muted = true; //Video won't start if not explicitly muted (i.e set video tag attribute muted is not enough)
			track.attach(elem);
			elem.play();


		}
	}

}

wildOsd.rpRegister("jitsiInit",jitsiInit);

function updateShips(args: string) {
	shipDataJson = JSON.parse(args);
	shipDataJson.forEach((d) => shipMap.set(d.id, d));
	if (useWebGl) {
		//Create the array to store the ships color index
		let polygonIndex = 0;
		let convexPolygonsShapes = new Array<Array<Array<number>>>(shipMap.size);
		let convexPolygonsPositions = new Array<Array<number>>(shipMap.size);
		let convexPolygonsColorsIndex = new Array<number>(shipMap.size);
		shipMap.forEach((d) => {
			const xy = WM_PROJ([d.lon, d.lat]);
			const x = xy[0] * TP_WIDTH;
			const y = xy[1] * TP_HEIGHT;
			let angle = 0;
			if (d.bearing != null && d.bearing != 0) {
				angle = d.bearing * Math.PI / 180 + HALF_PI;
			}
			convexPolygonsColorsIndex[polygonIndex] = parseInt(d.stype);
			convexPolygonsPositions[polygonIndex] = [x, y];
			convexPolygonsShapes[polygonIndex++] = generateShipSymbol(SHIP_SYMBOL_SIZE, angle);
		});
		shipOverlay.setConvexPolygons(convexPolygonsShapes, convexPolygonsPositions, convexPolygonsColorsIndex);

		if (updateLines) {

			let colorsIndexes = new Array<number>(shipTrailMap.size);
			let lines = new Array<Array<Array<number>>>(shipTrailMap.size);
			let lineIndex = 0;
			shipTrailMap.forEach((positions, key) => {
				let shipInfo = shipMap.get(key.toString());
				let stype = 0;
				if (shipInfo)
					stype = parseInt(shipInfo.stype);
				colorsIndexes[lineIndex] = stype;
				lines[lineIndex] = positions;
				lineIndex++;
			});
			shipOverlay.setLines(lines, colorsIndexes);
			updateLines = false;
		}

	}

	//Update d3 graph
	d3Names.forEach((d3Name) => {
		wildOsd.getWindow(d3Name)!.onSetTransform(0,0,0,0);
	});


	wildOsd.osdViewer.forceRedraw();
	console.log(shipDataJson.length + " -> " + shipMap.size);
}

wildOsd.rpRegister("updateShips", updateShips);


let scaleTimerId = 0;
// force a scale update first time

wildOsd.osdViewer.addHandler("zoom", () => {
	delayedScaleUpdate();
});


const delayedScaleUpdate = function () {
	if (scaleTimerId)
		clearTimeout(scaleTimerId); //Postpone previous delayedScaleUpdate call
	//Node and browser have different version of setTimeout
	// @ts-ignore
	scaleTimerId = setTimeout(() => {
		updateScaleInvariants();
	}, 500.0);
};
let NAV_SCALE: number = 1;

function updateScaleInvariants() {
	let shipsG = document.querySelector("#shipInfo");
	if (shipsG) {
		let rootG = shipsG.parentNode;
		if (rootG) {
			// inspired by https://stackoverflow.com/questions/38224875/replacing-d3-transform-in-d3-v4
			let baseVal = rootG['transform'].baseVal.consolidate();
			let {a, b, c, d, e, f} = baseVal.matrix;
			let scaleX = Math.sqrt(a * a + b * b);
			NAV_SCALE = 1 / scaleX;
			if (NAV_SCALE != 1) {
				// infoboxes
				let sibs = document.querySelectorAll(".shipInfoBox");
				for (let sib of sibs) {
					adjustInfoBoxTransform(sib);
				}
			}
		}
	}
}

function adjustInfoBoxTransform(el: Element) {
	let shipData: AllShipInfo = d3.select(el).datum();
	let xy = WM_PROJ([shipData.lon, shipData.lat]);
	let dx: number = xy[0] + shipData.ibxo * NAV_SCALE * InfoBox.SCALE_FACTOR;
	let dy: number = xy[1] + shipData.ibyo * NAV_SCALE * InfoBox.SCALE_FACTOR;
	el.setAttribute('transform',
		`translate(${dx},${dy}) scale(${NAV_SCALE * InfoBox.SCALE_FACTOR})`);
	adjustInfoBoxLinkGeom(el, dx, dy, false);
}

function getInfoBoxTransformFromShipPos(ll: number[]): string {
	let xy = WM_PROJ(ll);
	return `translate(${xy[0] + InfoBox.X_OFFSET * NAV_SCALE * InfoBox.SCALE_FACTOR},${xy[1] + InfoBox.Y_OFFSET * NAV_SCALE * InfoBox.SCALE_FACTOR}) scale(${NAV_SCALE * InfoBox.SCALE_FACTOR})`;
}

// input: infobox <g> ID,
// x,y are the translate parameters of that <g>'s transform
function adjustInfoBoxLinkGeom(ibGEl: Element, x: number, y: number,
							   updateOffset: boolean) {
	let ibG = d3.select(ibGEl);
	let shipData: AllShipInfo = ibG.datum();
	let xy = WM_PROJ([shipData.lon, shipData.lat]);
	if (updateOffset) {
		// infobox is being dragged, store custom offset to ship
		// which we want to be scale invariant
		shipData.ibxo = (x - xy[0]) / (NAV_SCALE * InfoBox.SCALE_FACTOR);
		shipData.ibyo = (y - xy[1]) / (NAV_SCALE * InfoBox.SCALE_FACTOR);
		ibG.select("line")
			.attr("x2", -shipData.ibxo)
			.attr("y2", -shipData.ibyo);
	} else {
		// infobox is merely being adjusted to remain scale invariant as
		// we zoom in/out. The custom offset should not be updated.
		ibG.select("line")
			.attr("x2", (xy[0] - x) / (NAV_SCALE * InfoBox.SCALE_FACTOR))
			.attr("y2", (xy[1] - y) / (NAV_SCALE * InfoBox.SCALE_FACTOR));
	}
}


function requestShipsDetailDisplay(d: LiveShipInfo | null) {
	if (d && d.name != "[SAT-AIS]") {
		//console.log("Asking to show info for "+ship.id);
		wildOsd.rpCall("broadcastShipDetailDisplay", [d.id], socketShipUpdate);
	}
}

function requestShipsDetailDisplayByCoordinates(geoTopLeft: Point, geoBottomRight: Point) {
	shipMap.forEach((d) => {
		if (d.lon <= geoBottomRight[0] &&
			d.lon >= geoTopLeft[0] &&
			d.lat <= geoTopLeft[1] &&
			d.lat >= geoBottomRight[1]) {
			requestShipsDetailDisplay(d);
		}
	});
}

function requestShipsPathDisplayByCoordinates(geoTopLeft: Point, geoBottomRight: Point) {
	shipMap.forEach((d) => {
		if (d.lon <= geoBottomRight[0] &&
			d.lon >= geoTopLeft[0] &&
			d.lat <= geoTopLeft[1] &&
			d.lat >= geoBottomRight[1]) {
			if (d.name != "[SAT-AIS]" && wildOsd.isMaster) {
				//console.log("Asking to show info for "+ship.id);
				wildOsd.rpCall("broadcastShipPathDisplay", [d.id], socketShipUpdate);
			}
		}
	});
}

function displayShipDetails(args: string) {
	let selectedShip;
	let allData: AllShipInfo;
	let shipData: StaticShipInfo = JSON.parse(args);
	console.log(args);
	selectedShip = shipMap.get(shipData.id);
	allData = {
		id: shipData.id,
		mmsi: shipData.mmsi,
		name: shipData.name,
		callsign: shipData.callsign,
		imgUrl: shipData.imgUrl,
		lat: selectedShip.lat,
		lon: selectedShip.lon,
		bearing: selectedShip.bearing,
		speed: selectedShip.speed,
		dwt: selectedShip.dwt,
		length: selectedShip.length,
		dest: selectedShip.dest,
		flag: selectedShip.flag,
		stype: selectedShip.stype,
		ibxo: InfoBox.X_OFFSET,
		ibyo: InfoBox.Y_OFFSET
	};

	let infoBoxG = d3.select("#shipInfo").selectAll(`g#id${selectedShip.id}`)
		.data([allData])
		.enter()
		.append("g")
		.attr("id", `id${selectedShip.id}`)
		.attr("transform", getInfoBoxTransformFromShipPos([allData.lon, allData.lat]))
		.classed("shipInfoBox", true);

	//Drag & drop
	// @ts-ignore
	let dragElement: SVGGElement = document.getElementById(`id${selectedShip.id}`);
	new OpenSeadragon.MouseTracker({
		element: dragElement,
		preProcessEventHandler: function (eventInfo) {
			switch (eventInfo.eventType) {
				case 'pointerdown':
				case 'pointerup':
					// prevent pointerdown/pointerup events from bubbling
					// viewer won't see these events
					eventInfo.stopPropagation = true;
			}
		}, dragHandler: function (e) {
			// @ts-ignore
			let delta = wildOsd.osdViewer.viewport.deltaPointsFromPixels(e.delta);
			translateInfoBox(dragElement, delta.x, delta.y);
			// broadcast translate
			wildOsd.rpCall("translateInfoBoxfromId",
				[dragElement.id, delta.x, delta.y]);
		}
	});


	// link to shipPath
	infoBoxG.data([allData])
		.append("line")
		.attr("x1", (d) => (InfoBox.WIDTH / 2))
		.attr("y1", (d) => (InfoBox.HEIGHT / 2))
		.attr("x2", (d) => (-InfoBox.X_OFFSET))
		.attr("y2", (d) => (-InfoBox.Y_OFFSET))
		.attr('stroke-width', `${SHIP_STROKE_SIZE}px`)
		.attr("class", (d) => (`IB${d.stype}ds`));
	// infobox background
	infoBoxG.append("rect")
		.attr("x", 0)
		.attr("y", 0)
		.attr("width", InfoBox.WIDTH)
		.attr("height", InfoBox.HEIGHT)
		.attr("class", (d) => (`IB${d.stype}lf IB${d.stype}ds`));
	// title (and ship type)
	infoBoxG.append("text")
		.attr("x", InfoBox.MARGIN_LEFT)
		.attr("y", 10)
		.attr("text-anchor", "start")
		.attr("class", "title")
		.attr("pointer-events", "none")
		.text((d) => (d.name));
	infoBoxG.append("text")
		.attr("x", InfoBox.WIDTH - InfoBox.MARGIN_RIGHT - 12)
		.attr("y", 10)
		.attr("text-anchor", "end")
		.attr("pointer-events", "none")
		.html((d) => (`${getShipTypeStr(d.stype)} - ${d.flag}`));
	infoBoxG.append("image")
		.attr("width", 9)
		.attr("x", InfoBox.WIDTH - InfoBox.MARGIN_RIGHT - 9)
		.attr("y", 4)
		.attr("xlink:href", (d) => (`${shipImgSourceUrl}/flags/${d.flag.toLowerCase()}.png`));
	// MMSI & callsign
	infoBoxG.append("text")
		.attr("x", InfoBox.MARGIN_LEFT)
		.attr("y", 20)
		.attr("class", (d) => (`IB${d.stype}df`))
		.attr("pointer-events", "none")
		.text("MMSI");
	infoBoxG.append("text")
		.attr("x", InfoBox.MARGIN_LEFT + 24)
		.attr("y", 20)
		.attr("pointer-events", "none")
		.text((d) => (d.mmsi));
	infoBoxG.append("text")
		.attr("x", InfoBox.WIDTH - InfoBox.MARGIN_RIGHT - 40)
		.attr("y", 20)
		.attr("text-anchor", "end")
		.attr("class", (d) => (`IB${d.stype}df`))
		.attr("pointer-events", "none")
		.text("Callsign");
	infoBoxG.append("text")
		.attr("x", InfoBox.WIDTH - InfoBox.MARGIN_RIGHT)
		.attr("text-anchor", "end")
		.attr("y", 20)
		.attr("pointer-events", "none")
		.text((d) => (d.callsign));
	// deadweight, flag, destination
	infoBoxG.append("text")
		.attr("x", InfoBox.MARGIN_LEFT)
		.attr("y", 30)
		.attr("pointer-events", "none")
		.text((d) => (getShipDimensionsStr(d.dwt, d.length)));
	infoBoxG.append("text")
		.attr("x", InfoBox.WIDTH - InfoBox.MARGIN_RIGHT)
		.attr("y", 30)
		.attr("text-anchor", "end")
		.attr("pointer-events", "none")
		.text((d) => (getDestinationStr(d.dest)));
	// lat/lon, bearing, speed
	infoBoxG.append("text")
		.attr("x", InfoBox.MARGIN_LEFT)
		.attr("y", 40)
		.attr("pointer-events", "none")
		.text((d) => (getShipLatLonStr(d)));
	infoBoxG.append("text")
		.attr("x", 2 * InfoBox.WIDTH / 3)
		.attr("y", 40)
		.attr("text-anchor", "middle")
		.attr("pointer-events", "none")
		.html((d) => (getShipBearingStr(d)));
	infoBoxG.append("text")
		.attr("x", InfoBox.WIDTH - InfoBox.MARGIN_RIGHT)
		.attr("y", 40)
		.attr("text-anchor", "end")
		.attr("pointer-events", "none")
		.html((d) => (getShipSpeedStr(d)));
	// ship image
	infoBoxG.append("image")
		.attr("x", (InfoBox.WIDTH - InfoBox.SHIP_THUMB_WIDTH) / 2)
		.attr("y", 45)
		.attr("id", `img${selectedShip.id}`)
		.attr("xlink:href", (d) => (`${shipImgSourceUrl}/${d.imgUrl}`))
		.attr("width", InfoBox.SHIP_THUMB_WIDTH);
	// close icon
	infoBoxG.append("image")
		.attr("x", InfoBox.WIDTH - 18)
		.attr("y", 3)
		.attr("xlink:href", "images/close@2x.png")
		.attr("width", 15).attr("height", 15)
		.on("pointerdown", (event, d) => (requestDiscardInfoBox(d.id)));
}

wildOsd.rpRegister("displayShipDetails", displayShipDetails);

interface LLT {
	id: string;
	stype: string;
	lat: number;
	lon: number;
	hh: number;
	mm: number;
}

let test = true;

function displayShipPath(args: string) {
	let tss: LLT[] = JSON.parse(args);
	if (tss.length <= 0) return;
	let positions = new Array<Array<number>>(tss.length);
	for (let i = 0; i < tss.length; i++) {
		let xy = WM_PROJ([tss[i].lon, tss[i].lat]);
		let x = xy[0] * TP_WIDTH;
		let y = xy[1] * TP_HEIGHT;
		positions[i] = [x, y];
	}
	if (shipTrailMap.has(tss[0].id)){
		let res:number[][] = shipTrailMap.get(tss[0].id)!;
		for (let pos of positions){
			res.push(pos);
		}
		shipTrailMap.set(tss[0].id, res);
	}
	else {
		shipTrailMap.set(tss[0].id, positions);
	}
	updateLines = true;
}

wildOsd.rpRegister("displayShipPath", displayShipPath);

function getShipLatLonStr(shipData: AllShipInfo): string {
	let res: string = (shipData.lat < 0) ? `${-shipData.lat} S` : `${shipData.lat} N`;
	return res + ((shipData.lon < 0) ? ` ${-shipData.lon} W` : ` ${shipData.lon} E`);
}

function getShipBearingStr(shipData: AllShipInfo) {
	return (shipData.bearing == 511) ? "N/A" : `${shipData.bearing}&deg;`;
}

function getShipSpeedStr(shipData: AllShipInfo): string {
	return `${shipData.speed / 10}kt`;
}

function getShipDimensionsStr(dwt: number, length: number): string {
	let dwtStr: string = (dwt) ? `${dwt}mt` : "";
	let lengthStr: string = (length) ? `${length}m` : "";
	if (dwtStr) {
		return (lengthStr) ? `${dwtStr} ${lengthStr}` : dwtStr;
	} else if (lengthStr) {
		return lengthStr;
	} else return "N/A";
}

function getDestinationStr(dest: string): string {
	return (dest) ? dest : "N/A";
}

function getShipTypeStr(st: string): string {
	return SHIP_TYPE[st];
}

function requestDiscardInfoBox(shipId: string) {
	if (shipId && wildOsd.isMaster) {
		wildOsd.rpCall("broadcastDiscardInfoBox", [shipId], socketShipUpdate);
	}
}

function discardInfoBox(shipId: string) {
	d3.select(`g#id${shipId}`).remove();
}

wildOsd.rpRegister("discardInfoBox", discardInfoBox);


function removeDiv(divId: string) {
	//console.log("removeDiv "+divId);
	let divToDestroy = document.getElementById(divId);
	if (divToDestroy) {
		divToDestroy.remove();
	}
}

wildOsd.rpRegister("removeDiv", removeDiv);

let checkShipClick = false;
let lastClickPos: Point = new Point();

wildOsd.osdViewer.addHandler('canvas-click', function (viewerEvent) {
	if (viewerEvent.position) {
		let elements = document.elementsFromPoint(viewerEvent.position.x, viewerEvent.position.y);
		for (let i = 0; i < elements.length; i++) {
			let element = elements[i];
			if (element.getAttribute("class") == PieMenu.pieSliceClassName) {
				pieMenu.click(element.id);
				return;
			}
		}
		if (viewerEvent.quick) {
			//Click and not drag
			checkShipClick = true;
			lastClickPos = viewerEvent.position;
			wildOsd.osdViewer.forceRedraw();
		}
	}
});

wildOsd.osdViewer.addHandler('canvas-key', function (viewerEvent) {
	if (viewerEvent['originalEvent']['type'] == 'keypress') {
		if (viewerEvent['originalEvent']['key'] == 'm') {
			if (wildOsd.isSelectionMode) {
				wildOsd.endSelectionMode()
			} else {
				if (selectionType == SelectionType.PositionHistory)
					selectionType = SelectionType.InfoBox;
				else
					selectionType = SelectionType.PositionHistory;
				wildOsd.startSelectionMode();
			}
		} else if (viewerEvent['originalEvent']['key'] == 'c') {
			console.log("Clear touch events");
			wildOsd.resetTouch();
		}
	}
});

//TODO move it in WildOsd
function pieMenuShow(x: number, y: number) {
	menuOverlay.setViewportPosition(x,y);
	pieMenu.show();
	wildOsd.osdViewer.forceRedraw()
	if (wildOsd.isMaster)
		wildOsd.rpCall("pieMenuShow", [x, y]);
}

wildOsd.rpRegister("pieMenuShow", pieMenuShow);

function pieMenuHide() {
	pieMenu.hide();
	wildOsd.osdViewer.forceRedraw()
	if (wildOsd.isMaster)
		wildOsd.rpCall("pieMenuHide", []);
}

wildOsd.rpRegister("pieMenuHide", pieMenuHide);

function pieMenuSetLayer(layerID: string) {
	pieMenu.setCurrentLayer(layerID);
	wildOsd.osdViewer.forceRedraw();
	if (wildOsd.isMaster)
		wildOsd.rpCall("pieMenuSetLayer", [layerID]);
}

wildOsd.rpRegister("pieMenuSetLayer", pieMenuSetLayer);


wildOsd.osdViewer.addHandler('canvas-double-click', function (viewerEvent) {
	if (viewerEvent.position) {
		let viewportPos = wildOsd.osdViewer.viewport.viewerElementToViewportCoordinates(viewerEvent.position);
		pieMenuShow((viewerEvent.position.x-pieMenuRadius)/sizeX, (viewerEvent.position.y-pieMenuRadius)/sizeY);
	}
})

function translateInfoBox(svgGElement: SVGGElement, x: number, y: number) {
	//@ts-ignore
	let matrix = svgGElement.transform.baseVal.consolidate().matrix;
	let {a, b, c, d, e, f} = matrix;
	let scaleX = Math.sqrt(a * a + b * b);
	// let scaleY = Math.sqrt(c * c + d * d);
	svgGElement.setAttribute("transform", `translate(${e + x},${f + y}) scale(${scaleX})`);
	adjustInfoBoxLinkGeom(svgGElement, e, f, true);
}

function translateInfoBoxfromId(id: string, x: number, y: number) {
	translateInfoBox(document.getElementById(id) as unknown as SVGGElement, x, y);
}

wildOsd.rpRegister("translateInfoBoxfromId", translateInfoBoxfromId);


function selectionEnd(imageTopLeft: Point, imagebottomRight: Point) {
	//Get total image size to normalize
	let imageSize = wildOsd.osdViewer.world.getItemAt(0).getContentSize();

	let geoTopLeft = WM_PROJ.invert([imageTopLeft.x / imageSize.x, imageTopLeft.y / imageSize.y]);
	let geoBottomRight = WM_PROJ.invert([imagebottomRight.x / imageSize.x, imagebottomRight.y / imageSize.y]);

	//console.log(`Selection geo ${geoTopLeft} -> ${geoBottomRight}` );

	switch (selectionType) {
		case SelectionType.InfoBox:
			if (wildOsd.isMaster){
				requestShipsDetailDisplayByCoordinates(geoTopLeft, geoBottomRight);
			}
			break;
		case SelectionType.PositionHistory:
			if (wildOsd.isMaster){
				requestShipsPathDisplayByCoordinates(geoTopLeft, geoBottomRight);
			}
			break;
		default:
			console.log("Unknown selectionType");
	}

	wildOsd.endSelectionMode();

	wildOsd.overlayRemove(wildOsd.SELECTION_ID);
	wildOsd.rpCall("overlayRemove", [wildOsd.SELECTION_ID]);
}


// Add Inria Logo to the top right hand corner
let logoDiv = document.createElement('div') as HTMLDivElement;
let logoInria = document.createElement('img') as HTMLImageElement;
logoDiv.appendChild(logoInria);
logoInria.src="images/logo_inria.svg";
logoInria.id = "logoInria";

let logoOverlay = new FixedOverlay(wildOsd, wildOsd.overlayRootDiv,logoInria);
logoOverlay.setViewportPosition(0.9,0.0);
logoOverlay.setViewportSize(0.1,0.1);
