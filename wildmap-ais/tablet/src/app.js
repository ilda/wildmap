"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const openseadragon_1 = __importDefault(require("openseadragon"));
const socket_io_client_1 = __importDefault(require("socket.io-client"));
const queryString = window.location.search;
const urlParams = new URLSearchParams(queryString);
console.log(`url parameters"${urlParams}`);
let id = "tablet";
let syncServerIp = "127.0.0.1";
if (urlParams.has('syncserver')) {
    syncServerIp = urlParams.get('syncserver');
}
let syncPort = 8080;
if (urlParams.has('syncport')) {
    syncPort = +urlParams.get('syncport');
}
let tileSourceUrl = "http://127.0.0.1:8081/data/bayeux.json";
if (urlParams.has('tilesource')) {
    // @ts-ignore
    tileSourceUrl = urlParams.get('tilesource');
}
let color = "blue";
if (urlParams.has('color')) {
    // @ts-ignore
    color = urlParams.get('color');
}
let viewer = openseadragon_1.default({
    id: "seadragon-viewer",
    tileSources: tileSourceUrl,
    showNavigator: false,
    showNavigationControl: false,
    visibilityRatio: 0,
    wrapHorizontal: false,
    debugMode: false
});
viewer.addHandler("viewport-change", () => {
    //Send new position/zoom to server
    let rect = viewer.viewport.getBounds();
    rpCall("dragmagTransform", [id, color, rect.x, rect.y, rect.width, rect.height]);
});
const socketSynchro = socket_io_client_1.default("http://" + syncServerIp + ":" + syncPort);
if (urlParams.has('id')) {
    id = urlParams.get('id');
}
socketSynchro.emit("ID", id);
function rpCall(func, args) {
    socketSynchro.emit("Rpc", { func: func, args: args });
}
exports.default = rpCall;
socketSynchro.on("Rpc", (funcToCall) => {
    let code = funcToCall.func;
    console.log("Rpc " + code + " " + funcToCall.args);
    window[code].apply(this, funcToCall.args);
});
