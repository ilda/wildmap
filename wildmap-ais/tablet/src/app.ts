import OpenSeadragon, {Point, Rect} from "openseadragon";
import io from "socket.io-client";



const queryString = window.location.search;
const urlParams = new URLSearchParams(queryString);

console.log(`url parameters"${urlParams}`);

let id = "tablet";

let syncServerIp = "127.0.0.1";
if (urlParams.has('syncserver'))
{
	syncServerIp = <string>urlParams.get('syncserver');
}


let syncPort = 8080;
if (urlParams.has('syncport'))
{
	syncPort = +<string>urlParams.get('syncport');
}





let tileSourceUrl:string = "http://127.0.0.1:8081/data/bayeux.json"

if (urlParams.has('tilesource'))
{
	// @ts-ignore
	tileSourceUrl=urlParams.get('tilesource');
}

let color:string = "blue"

if (urlParams.has('color'))
{
	// @ts-ignore
	color=urlParams.get('color');
}


let viewer: OpenSeadragon.Viewer =  OpenSeadragon(
	{
		id: "seadragon-viewer",
		tileSources: tileSourceUrl,
		showNavigator:  false,
		showNavigationControl: false,
		visibilityRatio: 0,
		wrapHorizontal: false,
		debugMode:  false
	}
);


viewer.addHandler("viewport-change", ()=>
{
	//Send new position/zoom to server
	let rect:OpenSeadragon.Rect = viewer.viewport.getBounds();
	rpCall("dragmagTransform",[id,color,rect.x,rect.y,rect.width,rect.height]);
});




const socketSynchro: SocketIOClient.Socket = io("http://"+syncServerIp+":"+syncPort);

if (urlParams.has('id'))
{
	id = <string>urlParams.get('id');
}
socketSynchro.emit("ID", id);


//Listen to remote call
interface Rpc {
	func:   string;
	args?: 	any[];
}


export default function rpCall(func: string, args: any[]) {
	socketSynchro.emit("Rpc",{func:func,args:args});
}
socketSynchro.on("Rpc", (funcToCall: Rpc) => {
	let code = funcToCall.func;
	console.log("Rpc "+code + " " + funcToCall.args);
	window[code].apply(this,funcToCall.args);
});







