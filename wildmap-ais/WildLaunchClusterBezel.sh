#wild parameters
wcol=4
wrow=4
wtile_w=15360
wtile_h=4320
packagefile="package.json"
nodetmppathL="/home/wild/tmp/wildmapL"
nodetmppathR="/home/wild/tmp/wildmapR"
nodeuserdirpathL="/home/wild/tmp/wildmapL"
nodeuserdirpathR="/home/wild/tmp/wildmapR"
servertmppath="/home/wild/tmp/wildmap/package.json"
serverip="192.168.0.2"
clientcachedirL="/media/ssd/cache/wildmap"
clientcachedirR="/media/ssd/cache/wildmap"
tileserverip="127.0.0.1"
nwpath="nw"
user=wild
wallxbezel=252
wallybezel=308


#app parameters
viewSyncPort=8080
serverport=8081
tileport=8082
tuioport=8083
shipUpdatePort=8084
# 16-level pyramid using ESRI below 8
esri=true
esriaddress=http://192.168.2.254:8086/arcgis #Use reverse proxy

shipSymbolSize=40
shipSymbolSizeMaster=10
shipStrokeSize=4
shipPathStrokeSize=8
infoBoxScale="10"
infoBoxScaleMaster="0.5"



echo "clear nw cache"

walldo rm -r $nodeuserdirpathL
walldo rm -r $nodeuserdirpathR
walldo rm -r $nodetmppathL
walldo rm -r $nodetmppathR
walldo rm -r $clientcachedirL
walldo rm -r $clientcachedirR

sleep 1
walldo mkdir $nodeuserdirpathL
walldo mkdir $nodeuserdirpathR
walldo mkdir $nodetmppathL
walldo mkdir $nodetmppathR
walldo mkdir $clientcachedirL
walldo mkdir $clientcachedirR

sleep 1

offsetX=0
offsetY=0

wallwidth=$(((wcol*wtile_w)+(wcol-1)*wallxbezel))
echo Total width including bezels $wallwidth px
wallheight=$(((wrow*wtile_h)+(wrow-1)*wallybezel))
echo Total height including bezels $wallheight px

halftile_w=$((wtile_w/2))



#master canvas size. MUST be the same ratio than the wall
masterwidth=$((wallwidth/20))
echo Master width $masterwidth px
masterheight=$((wallheight/20))
echo Master width $masterheight px

echo "start nw on nodes"
for col in {a..d}
do
  for row in {1..4}
    do

      echo  {\"name\": \"wildmapL\",\"node-remote\": \"http://*/*\",\"main\": "\"http://$serverip:$serverport/?id=$col$row&offsetx=$offsetX&offsety=$offsetY&wallx=$wcol&wally=$wrow&wallwidth=$wallwidth&wallheight=$wallheight&sizex=$halftile_w&sizey=$wtile_h&tilesource=http://$serverip:$tileport&syncserver=$serverip&viewSyncPort=$viewSyncPort&shipUpdatePort=$shipUpdatePort&ESRI=$esri&esriaddress=$esriaddress&shipSymbolSize=$shipSymbolSize&shipStrokeSize=$shipStrokeSize&shipPathStrokeSize=$shipPathStrokeSize&infoBoxScale=$infoBoxScale&posx=0\"",\"window\": {\"frame\": false,\"width\": $halftile_w,\"height\": $wtile_h,\"fullscreen\": false,\"toolbar\": false}} > $servertmppath$col$row
      scp $servertmppath$col$row $user@$col$row:$nodetmppathL/$packagefile
      ssh $user@$col$row DISPLAY=:0 $nwpath $nodetmppathL --disk-cache-dir=$clientcachedirL&
      sleep 0.2

      offsetXR=$((offsetX+halftile_w+wallxbezel))

      echo  {\"name\": \"wildmapR\",\"node-remote\": \"http://*/*\",\"main\": "\"http://$serverip:$serverport/?id=$col$row&offsetx=$offsetXR&offsety=$offsetY&wallx=$wcol&wally=$wrow&wallwidth=$wallwidth&wallheight=$wallheight&sizex=$halftile_w&sizey=$wtile_h&tilesource=http://$serverip:$tileport&syncserver=$serverip&viewSyncPort=$viewSyncPort&shipUpdatePort=$shipUpdatePort&ESRI=$esri&esriaddress=$esriaddress&shipSymbolSize=$shipSymbolSize&shipStrokeSize=$shipStrokeSize&shipPathStrokeSize=$shipPathStrokeSize&infoBoxScale=$infoBoxScale&posx=$halftile_w\"",\"window\": {\"frame\": false,\"width\": $halftile_w,\"height\": $wtile_h,\"fullscreen\": false,\"toolbar\": false}} > $servertmppath$col$row
      scp $servertmppath$col$row $user@$col$row:$nodetmppathR/$packagefile
      ssh $user@$col$row DISPLAY=:0 $nwpath $nodetmppathR --disk-cache-dir=$clientcachedirR&

      sleep 0.2
      offsetY=$((offsetY+wtile_h+wallybezel))
    done
    offsetX=$((offsetX+wtile_w+wallxbezel*2))
    offsetY=0
done

echo "Click on the link to launch master locally"

echo http://$serverip:$serverport/?id=MASTER\&sizex=$masterwidth\&sizey=$masterheight\&wallwidth=$wallwidth\&wallheight=$wallheight\&syncserver=$serverip\&viewSyncPort=$viewSyncPort\&shipUpdatePort=$shipUpdatePort\&tilesource=http://$tileserverip:$tileport\&tuioserver=$serverip\&tuioport=$tuioport\&ESRI=$esri\&infoBoxScale=$infoBoxScaleMaster\&shipSymbolSize=$shipSymbolSizeMaster
