viewSyncPort=8080
serverport=8081
tileport=8082
serverip="192.168.0.2"

nwpath="nw"

#tile path on the server
tiledirpath="/media/ssd/Demos/wildmap"
#tile path on the nodes
nodetiledirpath="/media/ssd/Demos/wildmap"

staticshipdatapath="/media/ssd/Demos/wildmap/staticShipData"

shipType=6
minShipLength=250
# wallsizeX/masterviewportX
onlyWithPic=true

echo "start node wildsyncserver for sync & tuio"
cd ./server && pm2 start --name "wildsyncserver" ./node_modules/wildsyncserver/lib/server.js -- --ip=$serverip

echo "start node server for ship update"
pm2 start --name "aisserver" ./dist/server.js -- --db=/home/wild/Demos/wildmap/wildmap-ais/server/data/database_MarineTraffic_2016-06-01.sqlite --shipsWithPic=$onlyWithPic --shipLength=$minShipLength --staticShipData=$staticshipdatapath

echo "start http server locally on port "$serverport
cd ../client && pm2 start npm --name "wildmaphttp" -- run http

echo "start http server for the tiles on port "$tileport
cd $tiledirpath && pm2 start serve --name "wildmaptile" -- -p $tileport

echo "start http server for the tiles on each node on port "$tileport
walldo serve $nodetiledirpath -p $tileport -C
