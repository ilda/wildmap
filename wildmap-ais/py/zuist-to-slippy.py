#!/usr/local/bin/python

# invert row file names to go back from ZUIST to slippy map format

# Usage: ./zuist-to-slippy.py <source_dir> <target_dir> <ext>
# target_dir will be created if it does not exist
# ext can be e.g. jpg or png

import os, sys, re
import shutil

SRC_DIR = sys.argv[1]
TGT_DIR = sys.argv[2]

EXT = sys.argv[3]

def processColumn(scd, tcd):
    #print("Processing %s" % scd)
    if not os.path.exists(tcd):
        os.mkdir(tcd)
    tiles = os.listdir(scd)
    for tile in tiles:
        tileNoExt = tile[:tile.find('.')]
        if tileNoExt:
            tileID = int(tileNoExt)
            itileID = len(tiles)-1-tileID
            shutil.copy("%s/%s.%s" % (scd, tileID, EXT) , "%s/%s.%s" % (tcd, itileID, EXT))
        else:
            print("Warning: unexpected file %s %s" % (scd, tile))

def processZoomLevel(szd, tzd):
    print("Processing %s" % szd)
    if not os.path.exists(tzd):
        os.mkdir(tzd)
    for f in os.listdir(szd):
        scd = "%s/%s" % (szd, f)
        tcd = "%s/%s" % (tzd, f)
        if os.path.isdir(scd):
            processColumn(scd, tcd)

if not os.path.exists(TGT_DIR):
    print("Creating %s" % TGT_DIR)
    os.mkdir(TGT_DIR)

for f in os.listdir(SRC_DIR):
    szd = "%s/%s" % (SRC_DIR, f)
    tzd = "%s/%s" % (TGT_DIR, f)
    if os.path.isdir(szd):
        processZoomLevel(szd, tzd)
