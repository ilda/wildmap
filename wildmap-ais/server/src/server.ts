const express = require('express');
const fs = require('fs');
import * as socketio from "socket.io";
import * as path from "path";
const chalk = require('chalk');
const csv = require('csv-parser');
const sqlite3 = require('sqlite3').verbose();
const yargs = require('yargs/yargs');
const { hideBin } = require('yargs/helpers');
const argv = yargs(hideBin(process.argv)).argv;

enum LogType {
	PLAIN = 0,
	INFO = 1,
	WARNING = 2,
	ERROR = 3
}

/*------------------------------------------*/
/* Logging                                  */
/*------------------------------------------*/

const log = function(msg:string, tl:number, ltype?:LogType){
	if (tl <= TRACE_LEVEL){
		if (typeof(ltype) !== 'undefined'){
			switch (ltype) {
				case LogType.INFO:{console.log(`INFO: ${chalk.blue(msg)}`);break;}
				case LogType.WARNING:{console.log(`WARNING: ${chalk.orange(msg)}`);break;}
				case LogType.ERROR:{console.log(`ERROR: ${chalk.red(msg)}`);break;}
				case LogType.PLAIN:{console.log(msg);break;}
			}
		}
		else {
			console.log(msg);
		}
	}
}

let TRACE_LEVEL:number = 10;
if (argv.tl){
	TRACE_LEVEL = argv.tl;
}

let SHIP_DATABASE_DIR:string = 'data/';
let SHIP_DATABASE_FILE:string = 'database_MarineTraffic_2016-06-01.sqlite';
if (argv.db){
	let lastDirSepIndex = argv.db.lastIndexOf('/');
	if (lastDirSepIndex == -1){
		SHIP_DATABASE_DIR = "";
		SHIP_DATABASE_FILE = argv.db;
	}
	else {
		SHIP_DATABASE_DIR = argv.db.substring(0,lastDirSepIndex+1);
		SHIP_DATABASE_FILE = argv.db.substring(lastDirSepIndex+1);
	}
	log(`Loading ship data from ${SHIP_DATABASE_DIR}${SHIP_DATABASE_FILE}`, 1, LogType.INFO);
}

let SHIP_DATABASE_STARTDATE:string = "2016-06-02T14:51:00";
if (argv.date){
	SHIP_DATABASE_STARTDATE =argv.date;
}

let SHIP_TYPE_SQL:number = 0;
if (argv.shipType){
	SHIP_TYPE_SQL = argv.shipType;
}

let SHIP_DWT_SQL:number = 0;
if (argv.shipDWT){
	SHIP_DWT_SQL = argv.shipDWT;
	log(`Minimum ship deadweight ${SHIP_DWT_SQL}mt`, 1, LogType.INFO);
}

console.log(argv)

let SHIP_LENGTH_SQL:number = 0;
if (argv.shipLength){
	SHIP_LENGTH_SQL = argv.shipLength;
	console.log(`Minimum ship length ${SHIP_LENGTH_SQL}m`, 1, LogType.INFO);
}

let SHIPS_WITH_PIC:boolean = false;
if (argv.shipsWithPic){
	SHIPS_WITH_PIC = argv.shipsWithPic;
	if (SHIPS_WITH_PIC){
		log("Only showing ships with a picture", 1, LogType.INFO);
	}
}

/*------------------------------------------*/
/* Ship data                                */
/*------------------------------------------*/

const DATABASE_CURRENT_DATE = {
	timeStep: new Date(SHIP_DATABASE_STARTDATE),
	updFreq: 5000, // in milliseconds
	incVal: 1 // in minutes
};

interface StaticShipInfo {
	id:string;
	name:string;
	mmsi:string;
	callsign:string;
	imgUrl:string;
};

interface LiveShipInfo {
	id:string;
	name:string;
	lat:number;
	lon:number;
	bearing:number;
	speed:number;
	dwt:number;
	length:number;
	flag:string;
	dest:string;
	stype:string;
	mmsi?:string;
	imgUrl?:string;
};

// actual ship temporal database
var MT_DB:any;

// static ship info
let STATIC_SHIP_DATA_DIR = "data/staticShipData";
if (argv.staticShipData){
	STATIC_SHIP_DATA_DIR = argv.staticShipData;
}
const STATIC_SHIP_INFO_CSV = `${STATIC_SHIP_DATA_DIR}/ships.csv`;
const STATIC_SHIP_IMG_CSV = `${STATIC_SHIP_DATA_DIR}/shipImgs.csv`;
const CSV_DELIMITER:string = ";";
const STATIC_SHIP_INFO = new Map<string,StaticShipInfo>();

const app = express();

const shipUpdatePort = 8084;



let shipUpdateServer = app.listen(shipUpdatePort, "0.0.0.0", () => {
	log(`Ship server listening on ${shipUpdatePort}`, 2, LogType.INFO);
});


app.get("/", (req: any, res: any) => {
	res.sendFile(path.resolve("./dist/index.html"));
});

let shipUpdateSocket = require('socket.io').listen(shipUpdateServer);

shipUpdateSocket.on("connect", (socket: socketio.Socket) => {
	socket.on("ID", (message: any) => {
		if (message === "MASTER") {
			log("Master listening for viewport updates", 2, LogType.INFO);
		} else {
			log(`Slave ${message} listening for viewport updates`, 2, LogType.INFO);
		}
	});

	socket.on("Rpc", (message: any) => {
		if (message['func'] == "broadcastShipDetailDisplay"){
			broadcastShipDetailDisplay(message.args);
		}
		else if (message['func'] == "broadcastShipPathDisplay"){
			broadcastShipPathDisplay(message.args);
		}
		else if (message['func'] == "broadcastDiscardInfoBox"){
			broadcastDiscardInfoBox(message.args);
		}
		else {
			// directly forward viewport transform commands to all clients
			 // console.log("Rpc -> "+JSON.stringify(message));
			//Forward message to slaves
			socket.broadcast.emit("Rpc",message);
		}

	});
});

shipUpdateSocket.on("connect", (socket: socketio.Socket) => {
	socket.on("ID", (message: any) => {
		if (message === "MASTER") {
			log("Master listening for ship updates", 2, LogType.INFO);
		} else {
			log(`Slave ${message} listening for ship updates`, 2, LogType.INFO);
		}
		// disabled, nothing to send for now
		// socket.emit("Rpc", {func:"initShips",
		// 					args:[{shipSymbolSize:SHIP_SYMBOL_SIZE,
		// 					       infoboxScaleFactor:INFOBOX_SCALE_FACTOR}]});
	});
});

// const server = http.listen(viewSyncPort, () => {
// 	console.log("listening for view sync clients on *:"+viewSyncPort);
// });

const addShipTypeCriterion = function():string{
	// using SHIPTYPE, not GT_SHIPTYPE for now
	// (latter sounds more detailed, but for later)
	return ;
};

const getInfoBoxSQLQueryExpression = function():string {
	let selectClause = `SELECT DISTINCT SHIP_ID id, SHIPNAME name, LAT lat, LON lon, HEADING heading, SPEED speed, SHIPTYPE stype, FLAG flag, DESTINATION dest, DWT dwt, LENGTH length`;
	let whereClause = `WHERE HOUR=${DATABASE_CURRENT_DATE.timeStep.getHours()} AND MINUTE=${DATABASE_CURRENT_DATE.timeStep.getMinutes()}`;
	if (SHIP_TYPE_SQL != 0){
		whereClause = whereClause + ` AND SHIPTYPE=${SHIP_TYPE_SQL}`;
	}
	if (SHIP_DWT_SQL != 0){
		whereClause = whereClause + ` AND DWT>=${SHIP_DWT_SQL}`;
	}
	if (SHIP_LENGTH_SQL != 0){
		whereClause = whereClause + ` AND LENGTH>=${SHIP_LENGTH_SQL}`;
	}
	return `${selectClause} FROM ships ${whereClause}`;
};

const updateShips = function(){
	let sql = getInfoBoxSQLQueryExpression();
	// console.log(`Querying:\n${sql}`);
	let ships:LiveShipInfo[] = [];
	MT_DB.all(sql, [], (err:any, rows:any) => {
		if (err){
			throw err;
		}
		log(`Found info for ${rows.length} ships at ${DATABASE_CURRENT_DATE.timeStep.getHours()}:${DATABASE_CURRENT_DATE.timeStep.getMinutes()}`, 2, LogType.PLAIN);
		for (let ship of rows){
			let shipId:string = ship['id'].toString(); // treated as a number
			let liveData:LiveShipInfo = {
				id: shipId,
				name: ship['name'],
				lat: parseFloat(ship['lat']),
				lon: parseFloat(ship['lon']),
				bearing: parseInt(ship['heading']),
				speed: parseFloat(ship['speed']),
				dwt: parseFloat(ship['dwt']),
				length: parseFloat(ship['length']),
				dest: ship['dest'],
				stype: (ship['stype']) ? ship['stype'] : '0',
				flag: ship['flag']
			};
			let staticData:StaticShipInfo = getStaticData(shipId);
			if (staticData){
				liveData.mmsi = staticData.mmsi;
				liveData.imgUrl = staticData.imgUrl;
			}
			if (!SHIPS_WITH_PIC || liveData.imgUrl){
				ships.push(liveData);
			}
		}
		// console.log(ships);
		sendShipUpdates(ships);
		incTime();
	});
};

const sendShipUpdates = function(ships:LiveShipInfo[]){
	let jsonShips = JSON.stringify(ships);
		shipUpdateSocket.emit("Rpc",
			{func:"updateShips", args:[jsonShips]});
};

const incTime = function(){
	DATABASE_CURRENT_DATE.timeStep = new Date(DATABASE_CURRENT_DATE.timeStep.getTime() + DATABASE_CURRENT_DATE.incVal*60000);
};

const getShipPathSQLQueryExpression = function(shipId:string){
	let selectClause = `SELECT SHIP_ID id, LAT lat, LON lon, HOUR hh, MINUTE mm, SHIPTYPE stype`;
	let whereClause = `WHERE SHIP_ID=${shipId}`;
	return `${selectClause} FROM ships ${whereClause}`;
};

const queryShipPath = function(shipId:string){
	log(`Querying ship path for ${shipId}`, 2, LogType.PLAIN);
	// console.log(`Querying:\n${sql}`);
	queryShipPathInNextFile(shipId, 1);
};

const queryShipPathInNextFile = function(shipId:string, dayIndex:number){
	let sqlFilePath:string = `${SHIP_DATABASE_DIR}database_MarineTraffic_2016-06-0${dayIndex}.sqlite`;
	let file_DB = new sqlite3.Database(sqlFilePath,
 		sqlite3.OPEN_READONLY, (err:any) => {
			if (err){
				log(err.message, 0, LogType.ERROR);
				return;
			}
			log(`Extracting ship path info from ${sqlFilePath}`, 2, LogType.PLAIN);
	});
	let sql = getShipPathSQLQueryExpression(shipId);
	let res:LLT[] = [];
	file_DB.all(sql, [], (err:any, rows:any) => {
		if (err){
			throw err;
		}
		log(`Found info for ${rows.length} time steps`, 2, LogType.PLAIN);
		if (rows.length == 0){return;}
		let plon = 1000; // values purposefully outside of range
		let plat = 1000; // to init the next loop
		let row = null;
		for (let i=0;i<rows.length;i++){
			row = rows[i];
			if (row.lon != plon || row.lat != plat){
				res.push({
					id: row['id'],
					stype: row['stype'],
					lat: parseFloat(row['lat']),
					lon: parseFloat(row['lon']),
					hh: parseFloat(row['hh']),
					mm: parseFloat(row['mm'])
				});
				plon = row.lon;
				plat = row.lat;
			}
		}
		if (res.length > 1){
			log(`Simplified ship path from ${rows.length} to ${res.length} points`, 2, LogType.PLAIN);
			sendShipPathForDisplay(res);
		}
		else {
			log(`No path to show for ${shipId}`, 1, LogType.INFO);
		}
		if (dayIndex < 9){
			queryShipPathInNextFile(shipId, dayIndex+1);
		}
	});
	file_DB.close();
};

const checkShipPath = function(shipPath:LLT[]):boolean{
	// check that ship path actually features some movement
	// (otherwise there will be nothing to show)
	let olat = shipPath[0].lat;
	let olon = shipPath[0].lon;
	//XXX could actually be a bit more clever and use a tolerance
	for (let llt of shipPath){
		if (olat != llt.lat || olon != llt.lon){
			return true;
		}
	}
	return false;
};

const loadShipInfo = function(){
	// parse local static data
	fs.createReadStream(STATIC_SHIP_INFO_CSV)
	  .on('error', (err:any) => {log(err, 0, LogType.ERROR);})
      .pipe(csv({ separator: ';' }))
      .on('data', (data:StaticShipInfo) => {addStaticShipDescription(data);})
      .on('end', () => {mapLocalImages();startShipUpdater();});
};

const mapLocalImages = function(){
	// replace Web URL to image with path to local file
	fs.createReadStream(STATIC_SHIP_IMG_CSV)
  	  .on('error', (err:any) => {log(err, 0, LogType.ERROR);})
      .pipe(csv({ separator: ';' }))
      .on('data', (data:any) => {rewriteImageLocation(data);});
};

const addStaticShipDescription = function(row:StaticShipInfo){
	STATIC_SHIP_INFO.set(row.id, row);
};

const rewriteImageLocation = function(row:any){
	let ship:StaticShipInfo = STATIC_SHIP_INFO.get(row.id);
	if (ship){
		ship.imgUrl = row.imgPath;
		STATIC_SHIP_INFO.set(ship.id, ship);
	}
};

const startShipUpdater = function(){
	// load SQL temporal database and start updating
	MT_DB = new sqlite3.Database(`${SHIP_DATABASE_DIR}${SHIP_DATABASE_FILE}`,
 		sqlite3.OPEN_READONLY, (err:any) => {
			if (err){
				log(err.message, 0, LogType.ERROR);
			}
			log(`Connected to MT DB ${SHIP_DATABASE_DIR}${SHIP_DATABASE_FILE}`, 2, LogType.INFO);
	});
	setInterval(updateShips, DATABASE_CURRENT_DATE.updFreq);
};

const stopShipUpdater = function(){
	MT_DB.close((err:any) => {
		if (err){
			log(err.message, 0, LogType.ERROR);
		}
		log("Disconnected from MT DB", 2, LogType.INFO);
	});
};

loadShipInfo();

/*------------------------------------------*/
/* Display ship information (scraping)      */
/*------------------------------------------*/

interface LLT {
	id:string;
	stype:string;
	lat:number;
	lon:number;
	hh:number;
	mm:number;
};

let broadcastShipDetailDisplay = function(args:any[]){
	const shipId:string = args[0];
	if (shipId){
		let si:StaticShipInfo = STATIC_SHIP_INFO.get(shipId);
		if (si){
			//console.log(`${si.name} ${si.imgUrl}`);
			sendShipDetailsForDisplay(si);
		}
	}
};

let broadcastShipPathDisplay = function(args:any[]){
	const shipId:string = args[0];
	if (shipId){
		queryShipPath(shipId);
	}
};

let getStaticData = function(shipId:string):StaticShipInfo|null{
	return STATIC_SHIP_INFO.get(shipId);
};

const sendShipDetailsForDisplay = function(ship:StaticShipInfo){
	let jsonShip = JSON.stringify(ship);
	shipUpdateSocket.emit("Rpc",
			{func:"displayShipDetails", args:[jsonShip]});
};

const sendShipPathForDisplay = function(tss:LLT[]){
	let jsonTss = JSON.stringify(tss);
	shipUpdateSocket.emit("Rpc",
		{func:"displayShipPath", args:[jsonTss]});
};

let broadcastDiscardInfoBox = function(shipId:string){
	shipUpdateSocket.emit("Rpc",
		{func:"discardInfoBox", args:[shipId]});
};
