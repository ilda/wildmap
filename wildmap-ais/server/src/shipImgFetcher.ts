export {};

const fs = require('fs');
const csv = require('csv-parser');
const axios = require('axios');
const pb = require('progress');

const srcDataFilePath = "data/ships.csv";
const imgDataDir = 'data';
const imgDataFilePath = `${imgDataDir}/shipImgs.csv`;

let csvAppender:any;

const CSV_DELIMITER:string = ";";
const CSV_HEADERS:string[] = ['id', 'imgPath'];
let CSV_HEADER_PRESENT = true;

interface ShipImg {
	id:string,
	imgPath:string
};

interface ShipInfo {
	id:string,
	name:string,
	mmsi:string,
	callsign:string,
	imgUrl:string
};

let processedShips:ShipImg[] = [];
let newShips:ShipInfo[] = [];

let row_i:number = 0;
let row_count:number;
let imgFetchTimer:any = null;

const getShipImages = async function(){
	csvAppender = fs.createWriteStream(imgDataFilePath, {flags:'a'});
	if (!CSV_HEADER_PRESENT){
		console.log(`Initializing ${imgDataFilePath}`);
		csvAppender.write(`${CSV_HEADERS.join(CSV_DELIMITER)}\n`);
	}
	fs.createReadStream(srcDataFilePath)
	  .on('error', () => {console.log(`${srcDataFilePath} not found`);})
	  .pipe(csv({ separator: ';' }))
	  .on('data', (data:ShipInfo) => {
		  newShips.push(data);
	  })
	  .on('end', () => {fetchNewShipImages();});
};

const fetchNewShipImages = function(){
	imgFetchTimer = setInterval(getNextShipImage, 200, [0]);
};

const getNextShipImage = function(inc:number){
	let hadToFetch:boolean = getShipImage(newShips[row_i].id,
		                                  newShips[row_i].imgUrl);
	row_i++;
	if (row_i % 10 == 0){console.log(`------------- Processed ${row_i} entries`);}
	if (row_i >= newShips.length){clearInterval(imgFetchTimer);}
	// go straight to next ship if this one didn't have to be fetched
	// (because already stored locally)
	if (!hadToFetch && inc < 500){
		getNextShipImage(inc+1);
	}
};

const getShipImage = function(shipId:string, imgUrl:string):boolean {
	if (processedShips.findIndex((ship) => (ship.id==shipId)) == -1){
		// ship has not yet been processed
		if (imgUrl){
			let pathToImg:string = fetchShipImage(shipId, imgUrl);
			csvAppender.write(`${[shipId,pathToImg].join(CSV_DELIMITER)}\n`);
			return true;
		}
		else {
			// no need to process
			return false;
		}
	}
	else {
		// ship was processed in a previous session
		return false;
	}
};

const fetchShipImage = function(shipId:string, imgUrl:string):string{
	// find out where to put it based on shipId
	// e.g. 2074573 will be stored in 2/0/2074573.jpg
	let c1:string = shipId.charAt(0);
	let c2:string = shipId.charAt(1);
	fs.mkdirSync(`${imgDataDir}/${c1}/${c2}`, {recursive: true});
 	let imgPath:string = `${imgDataDir}/${c1}/${c2}/${shipId}.jpg`;
	// do fetch it and store it there
	downloadImage(imgUrl, imgPath);
	// return the corresponding path
	return imgPath;
};

async function downloadImage (imgUrl:string, imgPath:string) {
  	const writer = fs.createWriteStream(imgPath);
  	const response = await axios({
     	url: imgUrl,
    	method: 'GET',
    	responseType: 'stream'
  	});

	const totalLength = response.headers['content-length'];

	const progressBar = new pb(`-> ${imgPath} [:bar] :percent :etas`, {
	      width: 40,
	      complete: '=',
	      incomplete: ' ',
	      renderThrottle: 1,
	      total: parseInt(totalLength)
	});

  	response.data.on('data', (chunk:any) => progressBar.tick(chunk.length));
	response.data.pipe(writer);


  	return new Promise((resolve, reject) => {
    	writer.on('finish', resolve)
    	writer.on('error', reject)
  	})
};

// // XXX https://stackoverflow.com/questions/37614649/how-can-i-download-and-save-a-file-using-the-fetch-api-node-js
// const downloadFile = (async (url:string, path:string) => {
// 	console.log(`${url} => ${path}`);
//   // const res:any = await fetch(url);
//   // const fileStream = fs.createWriteStream(path);
//   // await new Promise((resolve, reject) => {
//   //     res.body.pipe(fileStream);
//   //     res.body.on("error", reject);
//   //     fileStream.on("finish", resolve);
//   //   });
// });
// // https://static.vesselfinder.net/ship-photo/0-477995226-67e110162c9b7aeb347c7d720fe610af/1


let csvStreamReader = fs.createReadStream(imgDataFilePath)
						.on('error', () => {
							CSV_HEADER_PRESENT = false;
							console.log(`${imgDataFilePath} not found`);
							getShipImages();
						})
						.pipe(csv({ separator: ';' }))
						.on('data', (data:any) => {
							processedShips.push({id:data['id'], imgPath:''});
						})
						.on('end', () => {
							console.log(`Found ${processedShips.length} ships in ${imgDataFilePath}`);
							getShipImages();
						});
