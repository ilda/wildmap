export {};

const fs = require('fs');
const csv = require('csv-parser');
const sqlite3 = require('sqlite3').verbose();
let axios = require('axios');
let cheerio = require('cheerio');

// import os from 'os';
// console.log(os.hostname());
// console.log(`${os.type()}, ${os.platform()}, ${os.arch()}`);
//
console.log("Starting MMSI crawler");

// let execdir = import.meta.url;
// console.log(`Running from ${execdir}`);

const MT_FILE = 'data/database_MarineTraffic_2016-06-01.sqlite';

const VF_USER_AGENT = 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_2) AppleWebKit/601.3.9 (KHTML, like Gecko) Version/9.0.2 Safari/601.3.9';

// actual database
let MT_DB:any;
let ships:ShipInfo[] = [];

let row_i:number = 0;
let row_count:number;
let mmsiTimer:any;

const CSV_DELIMITER:string = ";";
const CSV_HEADERS:string[] = ['id', 'name', 'mmsi', 'callsign', 'imgUrl'];
let CSV_HEADER_PRESENT = true;

const dataFilePath = "data/ships.csv";

let csvAppender:any;

interface ShipInfo {
	id:string,
	name:string,
	mmsi:string,
	callsign:string,
	imgUrl:string
};

const getShips = async function(){
	csvAppender = fs.createWriteStream(dataFilePath, {flags:'a'});
	if (!CSV_HEADER_PRESENT){
		console.log(`Initializing ${dataFilePath}`);
		csvAppender.write(`${CSV_HEADERS.join(CSV_DELIMITER)}\n`);
	}
	MT_DB = new sqlite3.Database(MT_FILE,
 		sqlite3.OPEN_READONLY, (err:any) => {
			if (err){
				console.log(err.message);
			}
			console.log(`Connected to MT DB ${MT_FILE}`);
	});
	let sql = ` SELECT DISTINCT SHIP_ID id, SHIPNAME name FROM ships WHERE SHIPNAME != "[SAT-AIS]"`;
	console.log("Loading SQL...");
	MT_DB.all(sql, [], (err:any, rows:any) => {
		if (err){
			throw err;
		}
		console.log(`Found info for ${rows.length} ships`);
		row_count = rows.length;
		mmsiTimer = setInterval(getNextShipInfo, 200, rows);
	});
	MT_DB.close((err:any) => {
		if (err){
			console.log(err.message);
		}
		console.log("Disconnected from MT DB");
	});
};

let getNextShipInfo = function(rows:any){
	let hadToFetch:boolean = getShipInfo(rows[row_i].id, rows[row_i].name);
	row_i++;
	if (row_i % 100 == 0){console.log(`------------- Processed ${row_i} entries`);}
	if (row_i >= row_count){clearInterval(mmsiTimer);}
	// go straight to next ship if this one didn't have to be fetched
	// (because already stored locally)
	if (!hadToFetch){getNextShipInfo(rows);}
};

// returns false if ship was fetched in a previous session
let getShipInfo = function(shipId:string, shipName:string):boolean {
	if (shipId && ships.findIndex((ship) => (ship.id==shipId)) == -1){
		fetchShipMMSI(shipId, shipName);
		// just keep track of the fact that it has been processed in this session
		// in case it returned more than once by the SQL query
		ships.push({id:shipId, name:null, mmsi:null, callsign:null, imgUrl:null});
		return true;
	}
	else {
		// console.log(`${shipId} already stored`);
		return false;
	}
};

let parse_MMSI = function(title:string):string|null{
	if (title){
		// const MT_TITLE_PATTERN = /(\d+)/g;
		// let imo_mmsi = title.match(MT_TITLE_PATTERN);
		// if (imo_mmsi){return imo_mmsi.slice(0,2);}
		let mmsiIndex:number = title.indexOf("MMSI");
		if (mmsiIndex > -1){
			let fromMMSI:string = title.substring(mmsiIndex+5);
			let nextCommaIndex:number = fromMMSI.indexOf(",");
			if (nextCommaIndex > -1){
				return fromMMSI.substring(0, nextCommaIndex);
			}
		}
	}
	return null;
};

let parse_CallSign = function(title:string):string|null{
	if (title){
		// const MT_TITLE_PATTERN = /(\d+)/g;
		// let imo_mmsi = title.match(MT_TITLE_PATTERN);
		// if (imo_mmsi){return imo_mmsi.slice(0,2);}
		let csIndex:number = title.indexOf("Call Sign");
		if (csIndex > -1){
			let fromCS:string = title.substring(csIndex+10);
			return fromCS;
		}
	}
	return '';
};

let fetchShipMMSI = function(shipId:string, shipName:string) {
	let url:string = `https://www.marinetraffic.com/en/ais/details/ships/shipid:${shipId}`;
	//console.log(`Fetching MMSI for ${shipId} (${shipName})`);
	axios.get(url).then((response:any) => {
		let mtPage = cheerio.load(response.data);
		mtPage('title').each(function (i:number, e:any) {
		  	let title = mtPage(e).text();
			console.log(title);
		  	let mmsi:string =parse_MMSI(title);
			let callsign:string =parse_CallSign(title);
			console.log(`MMSI=${mmsi} CallSign=${callsign}`);
			fetchAndStoreShipDetails(shipId, shipName, mmsi, callsign);
	})}).catch(function (e:any) {
		console.log(`Error requesting ship info for MT ID ${shipId}`);
	});
}

let fetchAndStoreShipDetails = function(shipId:string, shipName:string,
	                                    mmsi:string, callsign:string) {
	// alternative is to use IMO `https://www.vesselfinder.com/en/vessels/VOS-TRAVELLER-IMO-${imo};
	if (mmsi){
		axios.get(`https://www.vesselfinder.com/en/vessels/VOS-TRAVELLER-MMSI-${mmsi}`,
			      { headers: {'User-Agent': VF_USER_AGENT}}).then((response:any) => {
					  const vfPage = cheerio.load(response.data);
					  const shipImageUrl = vfPage('a.img-holder.s0 img').attr('src');
					  if (shipImageUrl){
						  csvAppender.write(`${[shipId, shipName, mmsi, callsign, shipImageUrl].join(CSV_DELIMITER)}\n`);
					  }
					  else {
						  // console.log("Found no image");
						  csvAppender.write(`${[shipId, shipName, mmsi, callsign, ''].join(CSV_DELIMITER)}\n`);
					  }
			}).catch(function (e:any){
				// console.log(e);
				console.log(`Error requesting ship info for MMSI ${mmsi}`);
				csvAppender.write(`${[shipId, shipName, mmsi, callsign, ''].join(CSV_DELIMITER)}\n`);
			}
		);
	}
	else {
		console.log("No MMSI");
	}
};

let csvStreamReader = fs.createReadStream(dataFilePath)
						.on('error', () => {
							CSV_HEADER_PRESENT = false;
							console.log(`${dataFilePath} not found`);
							getShips();
						})
						.pipe(csv({ separator: ';' }))
						.on('data', (data:ShipInfo) => {ships.push(data);})
						.on('end', () => {
							console.log(`Found ${ships.length} ships in ${dataFilePath}`);
							getShips();
						});

// async function listFiles(){
//     let dir = '.';
//     if (process.argv[2]){
//         dir = process.argv[2];
//     }
//     try {
//         const files = await fs.readdir(dir);
//         for (let file of files){
//             console.log(file);
//         }
//     }
//     catch(err){
//         console.log(err);
//     }
// }
//
// listFiles().catch(err => {console.log(err);});
