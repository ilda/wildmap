
offsetX=0
offsetY=0
wcol=4
wrow=4
wtile_w=7680
wtile_h=2160
viewSyncPort=8080
serverport=8081
tileport=8082
tuioport=8083
shipUpdatePort=8084
nwpath="nw"
user=wild
wallwidth=30720
wallheight=8640
#proxyaddress="192.168.2.254:8086"

#master canvas size. MUST be the same ratio than the wall
masterwidth=3072
masterheight=864


packagefile="package.json"
nodetmppath="/home/wild/tmp/wildmap"
nodeuserdirpath="/home/wild/.cache/wildmap"

#change those values
servertmppath="/home/wild/tmp/wildmap/package.json"
serverip="192.168.0.2"
clientcachedir="/media/ssd/cache/wildmap"
tileserverip="127.0.0.1"

# 16-level pyramid using ESRI below 8
esri=true
esriaddress=http://192.168.2.254:8086/arcgis #Use reverse proxy


shipSymbolSize=2
shipSymbolSizeMaster=2
shipStrokeSize=4
shipPathStrokeSize=8
infoBoxScale="1"
infoBoxScaleMaster="0.1"



echo "clear nw cache"

walldo rm -r $nodeuserdirpath
walldo rm -r $nodetmppath
walldo rm -r $clientcachedir

sleep 1
walldo mkdir $nodetmppath
walldo mkdir $clientcachedir

sleep 1

echo "start nw on nodes"
for col in {a..d}
do
  for row in {1..4}
    do
      echo  {\"name\": \"wildmap\",\"main\": "\"http://$serverip:$serverport/?id=$col$row&offsetx=$offsetX&offsety=$offsetY&wallx=$wcol&wally=$wrow&wallwidth=$wallwidth&wallheight=$wallheight&sizex=$wtile_w&sizey=$wtile_h&tilesource=http://$serverip:$tileport&syncserver=$serverip&viewSyncPort=$viewSyncPort&shipUpdatePort=$shipUpdatePort&ESRI=$esri&esriaddress=$esriaddress&shipSymbolSize=$shipSymbolSize&shipStrokeSize=$shipStrokeSize&shipPathStrokeSize=$shipPathStrokeSize&infoBoxScale=$infoBoxScale\"",\"window\": {\"kiosk\": false,\"width\": 7680,\"height\": 2160}}} > $servertmppath$col$row
      scp $servertmppath$col$row $user@$col$row:$nodetmppath/$packagefile
      ssh $user@$col$row DISPLAY=:0 $nwpath $nodetmppath --disk-cache-dir=$clientcachedir&
      sleep 0.2
      offsetY=$((offsetY+wtile_h))
    done
    offsetX=$((offsetX+wtile_w))
    offsetY=0
done

echo "Click on the link to launch master locally"

echo http://$serverip:$serverport/?id=MASTER\&sizex=$masterwidth\&sizey=$masterheight\&wallwidth=$wallwidth\&wallheight=$wallheight\&syncserver=$serverip\&viewSyncPort=$viewSyncPort\&shipUpdatePort=$shipUpdatePort\&tilesource=http://$tileserverip:$tileport\&tuioserver=$serverip\&tuioport=$tuioport\&ESRI=$esri\&infoBoxScale=$infoBoxScaleMaster\&shipSymbolSize=$shipSymbolSizeMaster
