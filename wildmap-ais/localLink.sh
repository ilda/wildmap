#!/bin/bash

#connexion prameters
serverip="127.0.0.1"
viewSyncPort=8080
serverport=8081
dataport=8082
tuioport=8083
shipUpdatePort=8084

#client offset in pixel
offsetX=0
offsetY=0
#client screen size
clientscreenwidth=800
clientscreenheight=600

#master screen size
masterscreenwidth=800
masterscreenheight=600


#wall total size
wallwidth=3200
wallheight=2400

# 16-level pyramid using ESRI below 8
esri=true
tileext="png"

echo "start nw client"

echo "Client web page:" http://$serverip:$serverport/index.html?id=a1\&offsetx=$offsetX\&offsety=$offsetY\&wallwidth=$wallwidth\&wallheight=$wallheight\&sizex=$clientscreenwidth\&sizey=$clientscreenheight\&tilesource=http://$serverip:$dataport\&tileext=$tileext\&syncserver=$serverip\&syncport=$viewSyncPort\&ESRI=$esri

echo "Master web page:" http://$serverip:$serverport/index.html?id=MASTER\&sizex=$masterscreenwidth\&sizey=$masterscreenheight\&wallwidth=$wallwidth\&wallheight=$wallheight\&tilesource=http://$serverip:$dataport\&tileext=$tileext\&viewSyncPort=$viewSyncPort\&shipUpdatePort=$shipUpdatePort\&ESRI=$esri\&tuioserver=$serverip\&tuioport=$tuioport
