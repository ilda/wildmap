#!/bin/bash


offsetX=0
offsetY=0
screenwidth=15360
screenheight=4320


viewSyncPort=8080
serverport=8081
dataport=8082
shipUpdatePort=8084
serverip="127.0.0.1"

echo "start node server locally to synchonise nodes on port "$viewSyncPortport
cd ./server && pm2 start npm --name "wildzoomsync" -- start

echo "start http server locally on port "$serverport
http-server -p $serverport ../client/dist --cors=* > /dev/null  &

"/Applications/Google Chrome.app/Contents/MacOS/Google Chrome" --disable-logging --no-default-browser-check --aggressive-cache-discard  --start-fullscreen http://$serverip:$serverport/index.html?id=11\&offsetx=0\&offsety=0\&wallx=1\&wally=1\&sizex=1536\&sizey=432 &


echo "start chromium locally"


"/Applications/Google Chrome.app/Contents/MacOS/Google Chrome" --no-default-browser-check --aggressive-cache-discard http://$serverip:$serverport/index.html?id=MASTER\&sizex=1536\&sizey=432 &
