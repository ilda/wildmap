
echo "stop all servers"
pm2 stop aisserver wildmaphttp wildmaptile wildsyncserver

echo "delete all servers"
pm2 delete aisserver wildmaphttp wildmaptile wildsyncserver

echo "killall tile servers on nodes"
walldo killall node
