import pyvips


#parameters
tilewidth=1024
tileheight=1024

tiles=[]

top=0

for column in range(345):
  left=0
  for row in range(73):
    #print("vips draw_image $filename src/l_7/c_{}/tile_{}.jpg {} {}" .format(column, row, left, top))
    tiles.append(pyvips.Image.new_from_file("src/l_7/c_{}/tile_{}.jpg".format(column, row), access='sequential'))
    #target = target.draw_image(source, left, top)
    left=left + tilewidth
  top=top + tileheight

target = pyvips.Image.arrayjoin(tiles, across=5)
target.write_to_file('paris.png')
