# wildmap app for AIS data

AIS marine traffic visualization running on cluster-driven UHRWD using Web technologies.


Uses [OpenSeaDragon](https://openseadragon.github.io/) to display TMS tiles. Uses both WebGL and svg layers to display data on top of the tile pyramid.

## Installation

In both `server` and `client` directories, run:

```npm install```

```npm run compile```

Make sure that the serve module is installed on both the master and slave computers:

```npm install -g serve```

Make sure the pm2 module is installed on the master computer:

```npm install -g pm2```

## Usage

### URL parameters (client)

Example (master,local):

```http://127.0.0.1:8081/index.html?id=MASTER&sizex=800&sizey=600&tilesource=http://127.0.0.1:8082&ESRI=true&shipSymbolSize=100&infoBoxScale=2&shipStrokeSize=2&shipPathStrokeSize=2```

* Include ESRI tiles in the pyramid. Default: false.

```&ESRI=true```

* Ship symbol size (make them larger on wild than wilder). Default: 40.

```&shipSymbolSize=60```

* Ship stroke size (make it larger on wild than wilder). Default: 1.

```&shipStrokeSize=2```

* Ship path stroke size (make it larger on wild than wilder). Default: 1.

```&shipPathStrokeSize=2```

* infobox scale factor (make them larger on wild than wilder). Default: 1.

```&infoBoxScale=1.5```

### command line arguments (server)

* trace level (log) between 0 and 2, default=1

```--tl=2```

* path to local directory containing ship static data

```--staticShipData=path/to/dir```

* path to SQLite database containing ship temporal data

```--db=path/to/sqlite/file```

* time to start from in the database (YYYY-MM-DDTHH:MM:SS)

``` --date=2016-06-02T15:45 ```

* ship type to query (default=0, all ship types). See https://help.marinetraffic.com/hc/en-us/articles/205579997-What-is-the-significance-of-the-AIS-Shiptype-number- for allowed values.

``` --shipType=6 ```

* minimum ship deadweight to query, in metric tons (default=0, no minimum deadweight)

``` --shipDWT=10000 ```

* minimum ship length to query, in meters (default=0, no minimum length)

``` --shipLength=300 ```


* only display ships for which we can find a picture. Default: false.

```--shipsWithPic=true```

### Wild/er

* Launch sync and http server using pm2

```./WildLaunchServer.sh ```

* Launch NW.js on the cluster

```./WildLaunchCluster.sh ```

* Stop NW.js on the cluster

```walldo killall nw ```

* Stop sync and tile server using pm2

```./WildStopServer.sh ```

TODO: run the tile server

### local/dev

* Launch wildosd sync server using npm
  * ``` cd wildsyncserver && node lib/server.js --ip 192.168.x.xx```

* Launch sync and http server using npm
  * HTTP
    * First terminal
      * ``` cd client && npm run watch ```
    * Second terminal
      *   ``` cd client && npm run http ```
  * Sync
    * ``` cd server && npm run watch ```

* Launch tile server "serve"
    * Open a terminal and go to the folder where the tiles are
    * using serve:
      * ``` serve -p 8082 -C```

* Get Master and test Client links
  * Change parameters if needed (ip/port)
  * ``` ./localLink.sh ```
  * open links in two different tabs

