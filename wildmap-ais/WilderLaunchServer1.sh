viewSyncPort=8080
serverport=8081
tileport=8082
serverip="192.168.2.1"

nwpath="nw"

user=wild
#tile path on the server

tiledirpath="/media/ssd2To/Demos/wildmap"
#tile path on the nodes
nodetiledirpath="/media/ssd2To/Demos/wildmap"

shipType=6
minShipLength=210
onlyWithPic=true

source ~/.nvm/nvm.sh
nvm use v17.2.0

echo "start node wildsyncserver for sync & tuio"
cd ./server && pm2 start --name "wildsyncserver" ./node_modules/wildsyncserver/lib/server.js -- --ip=$serverip

echo "start node server for ship update"
pm2 start --name "aisserver" ./dist/server.js -- --db=/media/ssd2To/wildmap/wildmap-ais/server/data/database_MarineTraffic_2016-06-01.sqlite --shipsWithPic=$onlyWithPic --shipLength=$minShipLength --staticShipData=/media/ssd2To/Demos/wildmap/staticShipData

echo "start http server locally on port "$serverport
cd ../client && pm2 start npm --name "wildmaphttp" -- run http

echo "start http server for the tiles on port "$tileport
cd $tiledirpath && pm2 start serve --name "wildmaptile" -- -p $tileport

echo "start http server for the tiles on each node on port "$tileport
walldo serve $nodetiledirpath -p $tileport -C
