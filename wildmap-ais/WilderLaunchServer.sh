viewSyncPort=8080
serverport=8081
tileport=8082
serverip="192.168.2.2"

nwpath="nw"

user=wild
#tile path on the server

tiledirpath="/media/ssd2To/Demos/wildmap"
#tile path on the nodes
nodetiledirpath="/media/ssd2To/Demos/wildmap"

shipType=6
minShipLength=250
onlyWithPic=true

echo "start node server locally to synchonise nodes on port "
cd ./server && pm2 start --name "wildmapsync" ./dist/server.js -- --db=/media/data/Demos/wildmap/wildmap-osd/server/data/database_MarineTraffic_2016-06-01.sqlite --shipsWithPic=$onlyWithPic --shipLength=$minShipLength --staticShipData=/media/ssd2To/Demos/wildmap/staticShipData
#--shipType=$shipType
echo "start node server locally to forward tuio event "$tuioport
cd ../tuio && pm2 start npm --name "wildmaptuio" -- start -- -i $serverip

echo "start http server locally on port "$serverport
cd ../client && pm2 start npm --name "wildmaphttp" -- run http

echo "start http server for the tiles on port "$tileport
cd $tiledirpath && pm2 start serve --name "wildmaptile" -- -p $tileport

echo "start http server for the tiles on each node on port "$tileport
walldo serve $nodetiledirpath -p $tileport -C
