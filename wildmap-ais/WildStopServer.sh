
echo "stop all servers"
pm2 stop wildsyncserver wildmaphttp wildmaptile aisserver

echo "delete all servers"
pm2 delete wildsyncserver wildmaphttp wildmaptile aisserver

echo "killall tile servers on nodes"
walldo killall node
