# wildmap

Web-based map visualization on cluster-driven UHRWD.

Three different (independent) subprojects:
* wildmap-mapboxgl uses Mapbox GL JS v2
* wildmap-maplibre uses Maplibre
* wildmap-ais marine traffic visualization (AIS)
* wildmap-tle satellite orbit visualization (TLE)
