# wildmap for maplibre

Web-based map visualization on cluster-driven UHRWD.

Uses [Maplibre](https://maplibre.org)

## Installation

In both `server` and `client` directories, run:

```npm install```

```npm run compile```

Make sure that the http-server module is installed on both the master and slave computers:

```npm install -g http-server```

Make sure the pm2 module is installed on the master computer:

```npm install -g pm2```
