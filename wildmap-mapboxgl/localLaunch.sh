#!/bin/bash

offsetX=0
offsetY=0
wcol=3
wrow=2
wtile_w=600
wtile_h=300

appport=8080
serverport=8081
serverip="127.0.0.1"

tmpdir="/Users/epietrig/tmp/"
nodetmppath=$tmpdir"wildmap"

#nwpath="/usr/local/bin/nw"
nwpath="/Users/epietrig/Applications/nwjs-sdk-v0.54.0-osx-x64/nwjs.app/Contents/MacOS/nwjs"
ffpath="/Applications/Internet/Firefox.app/Contents/MacOS/firefox-bin"

echo "start local server to synchronize nodes on port "$appport
cd ./server && pm2 start npm --name "wildmapsync" -- start

echo "start local http server on port "$serverport
cd .. && http-server -p $serverport client/dist --cors=* > /dev/null  &

sleep 1

echo "clear nw cache"

rm -r $nodetmppath
sleep 1
mkdir $nodetmppath

sleep 1

echo "start client"

############## Using firefox

for col in {a..c}
do
	for row in {1..2}
      do
		$ffpath --new-window "http://$serverip:$serverport/index.html?id=$col$row&offsetx=$offsetX&offsety=$offsetY&wcol=$wcol&wrow=$wrow&wtile_w=$wtile_w&wtile_h=$wtile_h&syncserver=$serverip&syncport=$appport" &
        sleep 1
    	offsetY=$((offsetY+1))
      done
	offsetX=$((offsetX+1))
	offsetY=0
done


#
# $ffpath --new-window "http://$serverip:$serverport/index.html?id=a1&offsetx=$offsetX&offsety=$offsetY&wallx=4&wally=4&wallwidth=$wallwidth&wallheight=$wallheight&sizex=$screenwidth&sizey=$screenheight&syncserver=$serverip&syncport=$appport"
# sleep 2
# $ffpath --new-window "http://$serverip:$serverport/index.html?id=a2&offsetx=$offsetX&offsety=$offsetY&wallx=4&wally=4&wallwidth=$wallwidth&wallheight=$wallheight&sizex=$screenwidth&sizey=$screenheight&syncserver=$serverip&syncport=$appport"

############## Using nw.js
# echo  {\"name\": \"wildmap\",\"main\": "\"http://$serverip:$serverport/index.html?id=a1&offsetx=$offsetX&offsety=$offsetY&wallx=4&wally=4&wallwidth=$wallwidth&wallheight=$wallheight&sizex=$screenwidth&sizey=$screenheight&syncserver=$serverip&syncport=$appport\""} > $nodetmppath"/package.json"
# $nwpath $nodetmppath &
# echo "http://$serverip:$serverport/index.html?id=a1&offsetx=$offsetX&offsety=$offsetY&wallx=4&wally=4&wallwidth=$wallwidth&wallheight=$wallheight&sizex=$screenwidth&sizey=$screenheight&syncserver=$serverip&syncport=$appport"

echo "Master web page:" http://$serverip:$serverport/index.html?id=MASTER\&main_w=800\&main_h=600
