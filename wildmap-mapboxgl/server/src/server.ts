import * as express from "express";
import * as socketio from "socket.io";
import * as path from "path";

const app = express();

const port = 8080;
app.set("port", port);

let http = require("http").Server(app);
// set up socket.io and bind it to our
// http server.
let io = require("socket.io")(http);

let slaveConnections = new Map() ;
let masterConnection: socketio.Socket;


app.get("/", (req: any, res: any) => {
	res.sendFile(path.resolve("./dist/index.html"));
});

// whenever a user connects on port 8080 via
// a websocket, log that a user has connected
io.on("connection", (socket: socketio.Socket) => {
	socket.on("ID", (message: any) => {
		if (message === "MASTER") {
			masterConnection = socket;
			console.log("Master connected");
		} else {
			slaveConnections.set(message, socket);
			console.log(`Slave ${message} connected`);
		}
	});

	socket.on("Rpc", (message: any) => {
		console.log("Rpc");
		//Forward message to slaves
		socket.broadcast.emit("Rpc",message);


	});
});

const server = http.listen(port, () => {
	console.log("listening on *:"+port);
});
