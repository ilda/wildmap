#!/bin/bash

offsetX=0
offsetY=0
wcol=2
wrow=5
wtile_w=7680
wtile_h=960

appport=8080
serverport=8081
serverip="192.168.2.2"

nwpath="nw"

user=wild

packagefile="package.json"
servertmppath="/home/wild/tmp/wildmap/package.json"
nodetmppath="/home/wild/tmp/wildmap"
nodeuserdirpath="/home/wild/.cache/wildmap"

echo "start node server locally to synchonise nodes on port "$appport
cd ./server && pm2 start npm --name "wildmapsync" -- start

# echo "start node server locally to forward tuio event "$tuioport
# cd ../tuio && pm2 start npm --name "wildmaptuio" -- start -- -i $serverip

echo "start http server locally on port "$serverport
http-server -p $serverport ../client/dist --cors=* > /dev/null  &
#
# echo "start http server locally for tablet on port "$abletport
# http-server -p $tabletport ../tablet/dist --cors=* > /dev/null  &

sleep 1

echo "clear nw cache"

walldo rm -r $nodeuserdirpath
walldo rm -r $nodetmppath
sleep 1
walldo mkdir $nodetmppath

sleep 1

echo "start nw on nodes"
for col in {a..b}
do
  for row in {1..5}
    do

      echo  {\"name\": \"wildmap\",\"main\": "\"http://$serverip:$serverport/index.html?id=$col$row&offsetx=$offsetX&offsety=$offsetY&wcol=$wcol&wrow=$wrow&wti
le_w=$wtile_w&wtile_h=$wtile_h&syncserver=$serverip&syncport=$appport\"",\"window\": {\"kiosk\": true}} > $servertmppath$col$row
      scp $servertmppath$col$row $user@$col$row:$nodetmppath/$packagefile
      ssh $user@$col$row DISPLAY=:0 $nwpath $nodetmppath &
      sleep 0.2
      offsetY=$((offsetY+1))
      done
    offsetX=$((offsetX+1))
    offsetY=0
done

echo "start chromium locally"

echo DISPLAY=:1 chromium-browser --disable-web-security --user-data-dir=/home/wild/tmp --no-default-browser-check --aggressive-cache-discard http://$serverip:$
serverport/index.html?id=MASTER\&sizex=1536\&sizey=432\&syncserver=$serverip\&syncport=$appport

DISPLAY=:1 chromium-browser --disable-web-security --user-data-dir=/home/wild/tmp --no-default-browser-check --aggressive-cache-discard http://$serverip:$serve
rport/index.html?id=MASTER\&sizex=1536\&sizey=432\&syncserver=$serverip\&syncport=$appport  > /dev/null


