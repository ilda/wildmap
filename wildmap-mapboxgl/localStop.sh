#!/bin/bash

echo "kill node synchro server"
pm2 stop  "wildmapsync"

# echo "kill node tuio server"
# pm2 stop  "wildmaptuio"

echo "kill node on nodes"
killall nw
killall nwjs
killall firefox-bin

echo "kill http server on nodes"
killall http-server

echo "kill http server on server"
killall node

echo "Clean pm2"
# pm2 delete wildmapsync
