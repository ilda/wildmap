# wildmap for mapbox gl js v2

Web-based map visualization on cluster-driven UHRWD.

Uses [Mapbox GL JS](https://docs.mapbox.com/mapbox-gl-js/api/)

## Installation

In both `server` and `client` directories, run:

```npm install```

```npm run compile```

Make sure that the http-server module is installed on both the master and slave computers:

```npm install -g http-server```

Make sure the pm2 module is installed on the master computer:

```npm install -g pm2```
