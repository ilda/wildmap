import io from "socket.io-client";
import mapboxgl from "mapbox-gl";

const MAPBOX_KEY = 'pk.eyxxxxxxxx';

const queryString = window.location.search;
const urlParams = new URLSearchParams(queryString);

console.log(`url parameters"${urlParams}`);

let isMaster: boolean=true;
let id: string | null;

let syncServerIp = "127.0.0.1";
if (urlParams.has('syncserver'))
{
	syncServerIp = <string>urlParams.get('syncserver');
}


let syncPort = 8080;
if (urlParams.has('syncport'))
{
	syncPort = +<string>urlParams.get('syncport');
}

let tuioServerIp = "127.0.0.1";
if (urlParams.has('tuioserver'))
{
	tuioServerIp = <string>urlParams.get('tuioserver');
}

let tuioPort = 8083;
if (urlParams.has('tuioport'))
{
	tuioPort = +<string>urlParams.get('tuioport');
}


let offsetX:number = 1;
if (urlParams.has('offsetx'))
{
	// @ts-ignore
	offsetX = +urlParams.get('offsetx');
}
let offsetY:number = 1;
if (urlParams.has('offsety'))
{
	// @ts-ignore
	offsetY = +urlParams.get('offsety');
}

let wcol:number = 1.0;
if (urlParams.has('wcol'))
{
	// @ts-ignore
	wcol = +urlParams.get('wcol');
}
let wrow:number = 1.0;
if (urlParams.has('wrow'))
{
	// @ts-ignore
	wrow = +urlParams.get('wrow');
}

let wtile_w:number = 800.0;
let wtile_h:number = 600.0;

if (urlParams.has('wtile_w'))
{
	// @ts-ignore
	wtile_w = +urlParams.get('wtile_w');
}
if (urlParams.has('wtile_h'))
{
	// @ts-ignore
	wtile_h = +urlParams.get('wtile_h');
}



mapboxgl.accessToken = MAPBOX_KEY;
var el = document.getElementById("map-viewer");
if (el) {
	el.style.width = `${wtile_w}px`;
	el.style.height = `${wtile_h}px`;
	console.log(`Canvas size: ${wtile_w}px x ${wtile_h}px`);
	console.log(`Wall geom: ${offsetX},${offsetY} (${wcol},${wrow})`);
}

let amap = new mapboxgl.Map({
    container: "map-viewer",
    style: 'mapbox://styles/mapbox/outdoors-v11', // stylesheet location
    center: [2.1669545684936926, 48.71259152868464], // starting position [lng, lat]
    zoom: 16 // starting zoom
});

// let viewer: OpenSeadragon.Viewer =  OpenSeadragon(
// 	{
// 		id: "seadragon-viewer",
// 		tileSources: tileSourceUrl,
// 		showNavigator:  false,
// 		showNavigationControl: false,
// 		visibilityRatio: 0,
// 		wrapHorizontal: false,
// 		debugMode:  false,
// 		overlays: [{
// 			id: 'right-arrow-overlay',
// 			x: 0.6280,
// 			y: 0.1837,
// 			placement: 'RIGHT',
// 			checkResize: false
// 		}]
// 	}
// );



const socketSynchro: SocketIOClient.Socket = io("http://"+syncServerIp+":"+syncPort);

if (urlParams.has('id'))
{
	id = urlParams.get('id');
	socketSynchro.emit("ID", id);
	if (urlParams.get('id')==="MASTER")
		isMaster=true;
	else {
		isMaster=false;
	}
}
else
	socketSynchro.emit("message", "ID,ERROR");


//Listen to remote call
interface Rpc {
	func:   string;
	args?: 	any[];
}


export default function rpCall(func: string, args: any[]) {
	socketSynchro.emit("Rpc",{func:func,args:args});
}
socketSynchro.on("Rpc", (funcToCall: Rpc) => {
	let code = funcToCall.func;
	// console.log("Rpc "+code + " " + funcToCall.args);
	window[code].apply(this,funcToCall.args);
});

function viewportTransform(lng:number, lat:number, zoom:number){
	let xy_offset:[number,number] = [((wcol/2.0-0.5)*wtile_w)-offsetX*wtile_w,
		                             ((wrow/2.0-0.5)*wtile_h)-offsetY*wtile_h];
	amap.flyTo({center:[lng,lat], zoom: zoom, animate:false, offset:xy_offset});
	//XXX: the offset (in pixels) is what needs to be computed based on relative screen tile location
}
window["viewportTransform"] = viewportTransform; //Make this visible to be remotely called




interface tuioMessage{
	xPos:number,
	yPos:number,
	cursorId:number

}
let createEventDict = function (json: tuioMessage): PointerEventInit {
	const screenX = screen.width * json.xPos;
	const screenY = screen.height * json.yPos;
	// const clientX = (viewer.element.getBoundingClientRect().right - viewer.element.getBoundingClientRect().left) * json.xPos;
	// const clientY = (viewer.element.getBoundingClientRect().bottom - viewer.element.getBoundingClientRect().top) * json.yPos;

	return {
		pointerId: json.cursorId,
		// clientX: clientX,
		// clientY: clientY,
		screenX: screenX,
		screenY: screenY,
		pointerType: "touch",
		bubbles: true,
		cancelable: true,
		isPrimary: false
	};
};


if (isMaster){

	function updateViewport(){
		// let bounds:mapboxgl.LngLatBounds = amap.getBounds();
		var {lng, lat} = amap.getCenter();
		rpCall("viewportTransform", [lng, lat, amap.getZoom()]);
	}

	amap.on('move', function(){
		updateViewport();
	});

	let TouchNumber:number =0;

	// const socketInput: SocketIOClient.Socket = io("http://"+tuioServerIp+":"+tuioPort);
	//
	// socketInput.on("onAdd", (json: tuioMessage) => {
	// 	TouchNumber++;
	// 	console.log("onAdd "+TouchNumber);
	// 	let pointerEvent = new PointerEvent('pointerdown',
	// 		createEventDict(json));
	// 	// viewer.canvas.dispatchEvent(pointerEvent);
	// });
	//
	// socketInput.on("onUpdate", (json: tuioMessage) => {
	// 	console.log("onUpdate");
	// 	let pointerEvent = new PointerEvent('pointermove',
	// 		createEventDict(json));
	// 	// viewer.canvas.dispatchEvent(pointerEvent);
	//
	// });
	//
	// socketInput.on("onRemove", (json: tuioMessage) => {
	// 	TouchNumber--;
	// 	console.log("onRemove "+TouchNumber);
	// 	let pointerEvent = new PointerEvent('pointerup',
	// 		createEventDict(json));
	// 	// viewer.canvas.dispatchEvent(pointerEvent);
	// });
}
